package Tb_RxPktAnalyse;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_PktTransportParse ::*;
import RapidIO_RxPktFTypeAnalyse ::*;


module mkTb_RxPktAnalyse(Empty);

Ifc_RapidIO_PktTransportParse pktTransportParse <- mkRapidIO_PktTransportParse;
Ifc_RapidIO_RxPktFTypeAnalyse rxpktanalyse <- mkRapidIO_RxPktFTypeAnalyse;

Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 5)
		$finish (0);
	endrule




/////////////////////// ftype 5
rule r1(reg_ref_clk == 0);
	pktTransportParse._PktParseRx_SOF_n(False);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h004583004ff200000000000cffffffff);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r12(reg_ref_clk == 1);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(False);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'hffffffffffffffffffffffff00000000);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule rl_rxanalysemodule;
	rxpktanalyse._inputs_ReceivedPkts(pktTransportParse.outputs_ReceivedPkts_ ());
	rxpktanalyse._inputs_RxFtype5WriteClass(pktTransportParse.outputs_RxFtype5WriteClass_ ());
	rxpktanalyse._inputs_TTReceived(pktTransportParse.outputs_TTReceived_ ());
	rxpktanalyse._inputs_RxDestId(pktTransportParse.outputs_RxDestId_ ());
	rxpktanalyse._inputs_RxSourceId(pktTransportParse.outputs_RxSourceId_ ());
	rxpktanalyse._inputs_RxPrioField(pktTransportParse.outputs_RxPrioField_ ());
	rxpktanalyse._inputs_MaxPktCount(pktTransportParse.outputs_MaxPktCount_ ());
	rxpktanalyse._inputs_TxReady_From_IResp(True);
endrule

rule display;
	//$display("Output from packet transport parse module == %h",pktTransportParse.outputs_RxFtype5WriteClass_ ());
	$display("Target request output packets == %h",rxpktanalyse.outputs_TargetReqIfcPkt_ ());

$display("\n ################################# packet transport module outputs ###################################\n");
$display("ftype 5 class == %h",pktTransportParse.outputs_RxFtype5WriteClass_ ());
	$display("TT == %b",pktTransportParse.outputs_TTReceived_ ());
$display("Destination ID == %h",pktTransportParse.outputs_RxDestId_ ());
$display("Received packets == %h",pktTransportParse.outputs_ReceivedPkts_ ());
$display("Source ID == %h",pktTransportParse.outputs_RxSourceId_ ());
$display("Priority field == %h",pktTransportParse.outputs_RxPrioField_ ());
$display("Maximum packet count ==%b",pktTransportParse.outputs_MaxPktCount_ ());
endrule

endmodule

endpackage
