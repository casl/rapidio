/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Incoming Packet Parsing Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. After the incoming packets are separated as Header and Data Packets, it is forwarded 
--    to this module for further processing.
-- 2. The Header and Data packets are analyzed and logical layer ftype packets are generated.
-- 3. This module invokes RxFtypeFunctions package to depacketize the incoming packets.
-- 4. Supports both Dev8 and Dev16 fields. 
--
-- To Do's
-- Yet to Complete 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
--	-- Packet Description (Dev8 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:0], 8'h00 }
	Data_2 -> { Data2, 64'h0 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:8] }
	Data_1 -> { Data[7:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63:0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--      -- Packet Description (Dev16 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:8] }
	Data_1 -> { Data1[7:0], Data2 } 
	Data_2 -> { 8'h00, Data3 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:24] }
	Data_1 -> { Data[23:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63::0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--
--        
*/

package RapidIO_PktTransportParse;

import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import DefaultValue ::*;
import RapidIO_RxFTypeFunctionsDev8 ::*;
import RapidIO_RxFTypeFunctionsDev16 ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_TgtDecoder_ByteCnt_ByteEn ::*;


`include "RapidIO.defines"


interface Ifc_RapidIO_PktTransportParse;
 // Input Ports as Methods
 method Action _PktParseRx_SOF_n (Bool value); 
 method Action _PktParseRx_EOF_n (Bool value);
 method Action _PktParseRx_VLD_n (Bool value);
 method Bool link_rx_rdy_n_ ();

 method Action _PktParseRx_data (DataPkt value);
 method Action _PktParseRx_rem (Bit#(4) value);
 method Action _PktParseRx_crf (Bool value);

 method Action _inputs_TxReadyIn_From_Analyze (Bool value);

 
 method Maybe#(FType8_MaintenanceClass) outputs_RxFtype8MainReqClass_ ();
 method Maybe#(Data) outputs_RxFtype8MaintainData_ ();
 method ReceivedPktsInfo outputs_ReceivedPkts_ ();

 method TT outputs_TTReceived_ ();
 method DestId outputs_RxDestId_ ();
 method SourceId outputs_RxSourceId_ ();
 method Prio outputs_RxPrioField_ ();
 method Bit#(4) outputs_MaxPktCount_ ();

endinterface : Ifc_RapidIO_PktTransportParse

typedef struct { DataPkt headerpkt;
		 DataPkt datapkt;
		 Bit#(4) pktcount;
		 Bool lastpkt;
} ReceivedPktsInfo deriving (Bits, Eq);
instance DefaultValue#(ReceivedPktsInfo); // Default Value assigned for the Received PktsInfo 
   defaultValue = ReceivedPktsInfo {headerpkt: 0, datapkt: 0, pktcount: 0, lastpkt: False };
endinstance


(* synthesize *)

module mkRapidIO_PktTransportParse (Ifc_RapidIO_PktTransportParse);

 // Input Methods as Wires 
Wire#(Bool) wr_PktParseRx_SOF <- mkDWire (False);
Wire#(Bool) wr_PktParseRx_EOF <- mkDWire (False);
Wire#(Bool) wr_PktParseRx_VLD <- mkDWire (False);

Wire#(DataPkt) wr_PktParseRx_data <- mkDWire (0);
Wire#(Bit#(4)) wr_PktParseRx_rem <- mkDWire (0);
Wire#(Bool) wr_PktParseRx_crf <- mkDWire (False);

// Internal Wires and Registers
Wire#(DataPkt) wr_HeaderPkt <- mkDWire (0);
Reg#(DataPkt) rg_HeaderPkt <- mkReg (0); // Delay HeaderPkt for 1 clock cycle
Wire#(DataPkt) wr_DataPkt <- mkDWire (0);
Reg#(DataPkt) rg_DataPkt <- mkReg (0); // Delay DataPkt for 1 Clock Cycle
Wire#(Bit#(4)) wr_PktCount <- mkDWire (0);
Reg#(Bit#(4)) rg_PktCount <- mkReg (0);// Delay Pkt Count for 1 Clock Cycle
Reg#(Bool) rg_LastPkt <- mkReg (False); // Delayed the Last Packet valid bit 

Reg#(TT) rg_TTReceived <- mkReg (0); 
Reg#(Prio) rg_PrioReceived <- mkReg (0);
Reg#(DestId) rg_DestIDReceived <- mkReg (0);
Reg#(SourceId) rg_SrcIDReceived <- mkReg (0);
Reg#(Bit#(8)) rg_HopCountReceived <- mkReg (0);
Reg#(Maybe#(Bit#(4))) rg_RxRem <- mkReg (tagged Invalid);


Reg#(Maybe#(FType8_MaintenanceClass)) rg_Ftype8_MaintenanceClass <- mkReg (tagged Invalid);

Wire#(Bool) wr_TxReady_In <- mkDWire (False);

Ifc_RapidIO_InComingPkt_Separation pkt_Separation <- mkRapidIO_InComingPkt_Separation ();

rule rl_FromIncomingPktSeparation;
    	wr_HeaderPkt <= pkt_Separation.outputs_HeaderPkt_ ();
    	rg_HeaderPkt <= pkt_Separation.outputs_HeaderPkt_ (); // Delay the Header Packet
    	wr_DataPkt <= pkt_Separation.outputs_DataPkt_ ();
    	rg_DataPkt <= pkt_Separation.outputs_DataPkt_ (); // Delay the Data Packet 
    	wr_PktCount <= pkt_Separation.outputs_PktCount_ ();
    	rg_PktCount <= pkt_Separation.outputs_PktCount_ (); // Delay the Packet Count 
    	rg_LastPkt <= pkt_Separation.outputs_LastPkt_ (); // Delayed the Last Packet valid bit 
endrule

rule rl_HeaderDecode_IncomingPkt;
    if (pkt_Separation.outputs_PktCount_ () == 0) begin
	rg_DestIDReceived <= 0;
	rg_SrcIDReceived <= 0;
	rg_PrioReceived <= 0;
	rg_TTReceived <= 0; 
    end
    else begin
      	rg_TTReceived <= wr_HeaderPkt[117:116]; 
      	rg_PrioReceived <= wr_HeaderPkt[119:118];
      	if (wr_HeaderPkt[117:116] == 'b00) begin // will be corrected XXXXXXXXXXXXXXXXXXXXXXXXX 2'b00 for 8 bit device
		rg_DestIDReceived <= {wr_HeaderPkt[111:104], 24'h0};
		rg_SrcIDReceived <= {wr_HeaderPkt[103:96], 24'h0};
      	end
      	else if (wr_HeaderPkt[117:116] == 'b01) begin
		rg_DestIDReceived <= {wr_HeaderPkt[111:96], 16'd0};
		rg_SrcIDReceived <= {wr_HeaderPkt[95:80], 16'd0};
      	end 
      	else begin 
		rg_DestIDReceived <= {wr_HeaderPkt[111:104], 24'd0};
		rg_SrcIDReceived <= {wr_HeaderPkt[103:96], 24'd0};
      	end
    end 
endrule

rule rl_HopCountDecode;
    if ((wr_HeaderPkt[115:112] == `RIO_FTYPE8_MAINTAIN) && (wr_PktCount != 0))
	rg_HopCountReceived <= wr_HeaderPkt[79:72];
    else 
	rg_HopCountReceived <= 0;	
endrule

/*
-- Rule is to determine the values of the RxRem.
-- RxRem is valid only when the eof is enabled  
-- Physical layer (2 bytes ) removed and rg_RxRem contains the value of the RxRem for the logical layer
*/
rule rl_RxRemValid;
    if (wr_PktParseRx_EOF == True)
	rg_RxRem <= tagged Valid (wr_PktParseRx_rem - 'd2);
    else 
	rg_RxRem <= tagged Invalid;
endrule


/*
-- Maintenance Packet decoder uses temporary registers to hold the configuration offset.
-- Added logic for Dev8 and Dev16. 
*/

Reg#(Bit#(16)) rg_UpperBitOffset <- mkReg (0);
Wire#(Offset) wr_ConfigOffset <- mkDWire (0);
Reg#(Maybe#(Data)) rg_MaintenanceWrData <- mkReg (tagged Invalid);
rule rl_Ftype8MaintenanceRequestFormat; // Format Type 8


    if (wr_HeaderPkt[115:112] == `RIO_FTYPE8_MAINTAIN) begin
        if (wr_HeaderPkt[117:116] == 2'b00) begin  // Dev8 Support    	
    	    if ((wr_PktCount == 'd1) && ((wr_HeaderPkt[95:92] == 'd0) || (wr_HeaderPkt[95:92] == 'd3))) begin // Maintenance Packets without Data
	    	rg_Ftype8_MaintenanceClass <= tagged Valid fn_Dev8Ftype8MaintanenceRequestPkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt);
                rg_MaintenanceWrData <= tagged Invalid;
            end 
                     
            else if ((wr_PktCount == 'd2) && ((wr_HeaderPkt[95:92] == 'd1) || (wr_HeaderPkt[95:92] == 'd2))) begin // Maintenace Packets With Data 

                rg_Ftype8_MaintenanceClass <= tagged Valid fn_Dev8Ftype8MaintanenceRequestPkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt);
                Bit#(128) lv_TempData = {wr_HeaderPkt[47:0], wr_DataPkt[127:48]}; // Maintenance Data Decoding ...
	     	rg_MaintenanceWrData <= tagged Valid lv_TempData;
//$display("local variable data == %h",lv_TempData);
            end 

            else begin
	        rg_Ftype8_MaintenanceClass <= tagged Invalid;
	        rg_MaintenanceWrData <= tagged Invalid;
            end
        end  
        else if (wr_HeaderPkt[117:116] == 2'b01) begin // Dev16 Support 
            if ((wr_PktCount == 'd1) && ((wr_HeaderPkt[79:76] == 'd0) || (wr_HeaderPkt[79:76] == 'd3))) begin // Maintenance Packets without Data
	    	rg_Ftype8_MaintenanceClass <= tagged Valid fn_Dev16Ftype8MaintanenceRequestPkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt);
            end
 
            else if ((wr_PktCount == 'd2) && ((wr_HeaderPkt[79:76] == 'd1) || (wr_HeaderPkt[79:76] == 'd2))) begin // Maintenace Packets With Data
                rg_Ftype8_MaintenanceClass <= tagged Valid fn_Dev16Ftype8MaintanenceRequestPkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt); 
                Bit#(128) lv_TempData = {wr_HeaderPkt[31:0], wr_DataPkt[127:32]};
	     	rg_MaintenanceWrData <= tagged Valid lv_TempData; // Maintenance Data Decoding ...
            end

            else begin
	        rg_Ftype8_MaintenanceClass <= tagged Invalid;
	        rg_MaintenanceWrData <= tagged Invalid;
            end
        end 
    end
    else begin
	rg_Ftype8_MaintenanceClass <= tagged Invalid;
	rg_MaintenanceWrData <= tagged Invalid;
    end

endrule
/*
rule display;
	$display("wire packet counnt == %h\n",wr_PktCount);
endrule
*/
// Module Definition 
 // Input Ports as Methods
 method Action _PktParseRx_SOF_n (Bool value); 
	pkt_Separation._inputs_SOF (!value);
 endmethod
 method Action _PktParseRx_EOF_n (Bool value);
	pkt_Separation._inputs_EOF (!value);
 endmethod
 method Action _PktParseRx_VLD_n (Bool value);
	pkt_Separation._inputs_VLD (!value);
 endmethod
/* method Bool link_rx_rdy_n_ (); // Need to Implement
	return (wr_TxReady_In); 
 endmethod*/

 method Action _PktParseRx_data (DataPkt value);
	pkt_Separation._inputs_DataPkt (value);
 endmethod

 method Action _PktParseRx_rem (Bit#(4) value); // Need to Implement
	wr_PktParseRx_rem <= value; 
 endmethod

 method Action _PktParseRx_crf (Bool value); // Need to Implement
	wr_PktParseRx_crf <= value; 
 endmethod

 method Action _inputs_TxReadyIn_From_Analyze (Bool value);
	wr_TxReady_In <= value; 
 endmethod 

 // Output Methods

 method Maybe#(FType8_MaintenanceClass) outputs_RxFtype8MainReqClass_();
   if (rg_Ftype8_MaintenanceClass matches tagged Valid .ftype8MaintenanceClass)
//    	return rg_Ftype8_MaintenanceClass;
	return tagged Valid ftype8MaintenanceClass;
   else 
	return tagged Invalid; 
 endmethod
 method Maybe#(Data) outputs_RxFtype8MaintainData_();
   if (rg_MaintenanceWrData matches tagged Valid .ftype8MaintainData)
//	return rg_MaintenanceWrData;
	return tagged Valid ftype8MaintainData; 
   else 
	return tagged Invalid; 
 endmethod

 method ReceivedPktsInfo outputs_ReceivedPkts_ ();
	return ReceivedPktsInfo {headerpkt: rg_HeaderPkt, datapkt: rg_DataPkt, pktcount: rg_PktCount, lastpkt: rg_LastPkt };
 endmethod
 
 method TT outputs_TTReceived_ ();
	return rg_TTReceived; 
 endmethod
 method DestId outputs_RxDestId_ ();
	return rg_DestIDReceived;
 endmethod
 method SourceId outputs_RxSourceId_ ();
	return rg_SrcIDReceived;
 endmethod
 method Prio outputs_RxPrioField_ ();
	return rg_PrioReceived;
 endmethod
 method Bit#(4) outputs_MaxPktCount_ ();
	return pkt_Separation.outputs_MaxPktCount_ ();
 endmethod


endmodule : mkRapidIO_PktTransportParse

endpackage : RapidIO_PktTransportParse
