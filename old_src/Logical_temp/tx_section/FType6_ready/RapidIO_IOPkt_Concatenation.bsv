/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. Generates logical layer Packets for various format types
-- 2. Depend on the input Ftype value and SOF, the ftype packets are assigned with the
--    data signals 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package RapidIO_IOPkt_Concatenation;

import DefaultValue ::*;

import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;

`include "RapidIO.defines"

interface Ifc_RapidIO_IOPktConcatenation;

 method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt pkt); 
 method Action _inputs_TargetRespIfcPkt (TargetRespIfcPkt pkt);
 method Action _inputs_MaintenanceIfcPkt (MaintenanceRespIfcPkt pkt); 
 method Action _inputs_RxReady_From_IOGeneration (Bool value); 
 method FType6_StreamWrClass outputs_Ftype6_IOStreamWrClassPacket_ (); 
 method InitiatorReqIfcPkt outputs_InitReqIfcPkt_ (); 
 method TargetRespIfcPkt outputs_TgtRespIfcPkt_ ();
 method MaintenanceRespIfcPkt outputs_MaintainRespIfcPkt_ ();
 method InitReqDataInput outputs_InitReqDataCount_ ();
 method Bool outputs_RxRdy_From_Concat_ ();

endinterface : Ifc_RapidIO_IOPktConcatenation


typedef struct {Bool lastdata;
		Bit#(4) datacount;
} InitReqDataInput deriving (Bits, Eq);

instance DefaultValue#(InitReqDataInput); 
    defaultValue = InitReqDataInput {lastdata: False, datacount: 0};
endinstance

function ByteCount fn_ByteCountRoundOff (ByteCount value);
    if (value > 'd72)
	return 'd80;
    else if (value > 'd64)
	return 'd72;
    else if (value > 'd56)
	return 'd64;
    else if (value > 'd48)
	return 'd56;
    else if (value > 'd40)
	return 'd48;
    else if (value > 'd32)
	return 'd40;
    else if (value > 'd24) 
	return 'd32;
    else if (value > 'd16)
	return 'd24;
    else if (value > 'd8)
	return 'd16;
    else
	return 'd8; 
endfunction 

(* synthesize *)

module mkRapidIO_IOPktConcatenation (Ifc_RapidIO_IOPktConcatenation);

	Wire#(InitiatorReqIfcPkt) wr_InitReqIfcPkt <- mkDWire (defaultValue); 
	Wire#(TargetRespIfcPkt) wr_TgtRespIfcPkt <- mkDWire (defaultValue); 
	Wire#(MaintenanceRespIfcPkt) wr_MaintenanceRespIfcPkt <- mkDWire (defaultValue);

	Wire#(Bool) wr_RxReadyIn <- mkDWire (False);

	Reg#(Bool) rg_LastData <- mkReg (False);
	Reg#(Bit#(4)) rg_DataCount <- mkReg (0); 
	 
	Wire#(Bool) wr_IreqSOF <- mkDWire (False);
	Wire#(Bool) wr_IreqEOF <- mkDWire (False);

	Wire#(Bool) wr_Read <- mkDWire (False);

	Wire#(InitiatorReqIfcPkt) wr_InitReqIfc <- mkDWire (defaultValue);
	 
	Wire#(Bit#(`RIO_DATA_16)) wr_x <- mkDWire (0);

	Wire#(FType6_StreamWrClass) wr_Ftype6_StreamWrPkt <- mkDWire (defaultValue);

	Ifc_RapidIO_InitEncoder_WdPtr_Size mod_ConvertByteCountToSizeWdPtr <- mkRapidIO_InitEncoder_WdPtr_Size;

rule rl_ImportInputInterface;
  wr_IreqSOF <= wr_InitReqIfcPkt.ireqcntrl.ireq_sof;
  wr_IreqEOF <= wr_InitReqIfcPkt.ireqcntrl.ireq_eof;
endrule

rule rl_CheckWhetherRdorWr;
if (wr_InitReqIfcPkt.ireqdata.ireq_ftype ==  `RIO_FTYPE6_STREAM_WR) 
	wr_Read <= False;
endrule

rule rl_IfctoGenerateSizeWdPtr ((wr_IreqSOF == True) && (wr_InitReqIfcPkt.ireqdata.ireq_ftype != `RIO_FTYPE6_STREAM_WR));
  Bit#(9) lv_RndOff_ByteCount = fn_ByteCountRoundOff(wr_InitReqIfcPkt.ireqdata.ireq_byte_count);
  mod_ConvertByteCountToSizeWdPtr._inputs_Read (wr_Read);
  mod_ConvertByteCountToSizeWdPtr._inputs_ByteCount (wr_InitReqIfcPkt.ireqdata.ireq_byte_count); 
  mod_ConvertByteCountToSizeWdPtr._inputs_ByteEn (wr_InitReqIfcPkt.ireqdata.ireq_byte_en); 
endrule


rule rl_Ftype6StreamWriteClass;
  if ((wr_IreqSOF == True) && (wr_InitReqIfcPkt.ireqdata.ireq_ftype == `RIO_FTYPE6_STREAM_WR))
	     wr_Ftype6_StreamWrPkt <= FType6_StreamWrClass  {tt: wr_InitReqIfcPkt.ireqdata.ireq_tt, 
							                             ftype: wr_InitReqIfcPkt.ireqdata.ireq_ftype,
							                             addr: wr_InitReqIfcPkt.ireqdata.ireq_addr[47:3],
							                             xamsbs: wr_InitReqIfcPkt.ireqdata.ireq_addr[49:48]};
  else
	     wr_Ftype6_StreamWrPkt <= defaultValue;
endrule

rule rl_DataValidCount;
    if (wr_InitReqIfcPkt.ireqcntrl.ireq_vld == True) begin 
		if (wr_InitReqIfcPkt.ireqcntrl.ireq_sof == True) begin 
			rg_DataCount <= 'd1;
			rg_LastData <= False;
		end
	else if (wr_InitReqIfcPkt.ireqcntrl.ireq_eof == True) begin 
	    rg_DataCount <= rg_DataCount + 'd1;
	    rg_LastData <= True;	
	    end
	else begin 
	    rg_DataCount <= rg_DataCount + 'd1;
	    rg_LastData <= False;
	     end
      end
    else begin 
	rg_DataCount <= 0;
	rg_LastData <= False;
    end
endrule

 method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt pkt);
	wr_InitReqIfcPkt <= pkt;
	wr_InitReqIfc <= pkt;
 endmethod
 
 method Action _inputs_RxReady_From_IOGeneration (Bool value);
	wr_RxReadyIn <= value; 
 endmethod 

 method FType6_StreamWrClass outputs_Ftype6_IOStreamWrClassPacket_ ();
	return wr_Ftype6_StreamWrPkt;
 endmethod

 method InitiatorReqIfcPkt outputs_InitReqIfcPkt_ ();
	return wr_InitReqIfc;
 endmethod
 
 method InitReqDataInput outputs_InitReqDataCount_ ();
	return InitReqDataInput {lastdata: rg_LastData, datacount: rg_DataCount};
 endmethod

 method Bool outputs_RxRdy_From_Concat_ ();
	return wr_RxReadyIn;
 endmethod 	
endmodule : mkRapidIO_IOPktConcatenation

endpackage : RapidIO_IOPkt_Concatenation

