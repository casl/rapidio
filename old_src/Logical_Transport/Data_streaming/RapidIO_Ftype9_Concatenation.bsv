/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Format Type (Ftype) Definition Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module,
-- 
-- 
--
-- Author(s):
-- Guruprasad, Yadhava Krishnan, Ajay Kumar, Navin Sridharan
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIO_Ftype9_Concatenation;

import DefaultValue ::*;

import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;

`include "RapidIO.defines"

interface Ifc_RapidIO_Ftype9_Concatenation; 
 method FType9_DataStreamingClass outputs_Ftype9_DataStreamingClass_();
 method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt pkt);
 method InitiatorReqIfcPkt outputs_InitReqIfcPkt_ ();
endinterface : Ifc_RapidIO_Ftype9_Concatenation

(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_Ftype9_Concatenation (Ifc_RapidIO_Ftype9_Concatenation);
//register to initiate bytecount (included for ftype9
Reg#(ByteCount) rg_Byte_count<- mkReg(0);
Reg#(Type) rg_Ftype <- mkReg(0);
Reg#(Bool) rg_validate <- mkReg(False);
/*Wire#(COS) wr_cos <- mkReg(0);
Wire#(SRL) wr_srl <- mkReg(0);*/
// Input Methods as Wires
Wire#(InitiatorReqIfcPkt) wr_InitReqIfcPkt <- mkDWire (defaultValue);
Reg#(InitiatorReqIfcPkt) rg_InitReqInput1Delay <-mkReg(defaultValue);
Reg#(InitiatorReqIfcPkt) rg_InitReqInput2Delay <-mkReg(defaultValue);
// Initiator Request Signals
Wire#(Bool) wr_IreqSOF <- mkDWire (False);
Wire#(Bool) wr_IreqEOF <- mkDWire (False);
Reg#(InitiatorReqIfcPkt) rg_testinp <- mkReg(defaultValue);
Wire#(InitiatorReqIfcPkt) wr_InitReqIfc <- mkDWire (defaultValue);
Wire#(FType9_DataStreamingClass) wr_Ftype9_DataStreamingPkt <- mkDWire (defaultValue);


// -- Rules
rule rl_initBytecount;
	if(wr_InitReqIfcPkt.ireqcntrl.ireq_sof==True) begin 
		rg_Byte_count <= wr_InitReqIfcPkt.ireqdata.ireq_byte_count;
		rg_Ftype <= wr_InitReqIfcPkt.ireqdata.ireq_ftype;
	end
	else if (wr_InitReqIfcPkt.ireqcntrl.ireq_eof==True) begin
		rg_Byte_count <= 0;
		rg_Ftype <= 0;
	end
endrule

///////////////////////////////////////////////
/* Rule Definition for concatenation ftype 9
	for different kinds of segment */
//////////////////////////////////////////////
rule rl_Ftype9DatastreamingClass((wr_InitReqIfcPkt.ireqcntrl.ireq_sof == True)&&(wr_InitReqIfcPkt.ireqcntrl.ireq_vld==True) && (wr_InitReqIfcPkt.ireqdata.ireq_ftype == `RIO_FTYPE9_DATASTREAM) && (wr_InitReqIfcPkt.ireqdata.ireq_byte_count <= 8) && (wr_InitReqIfcPkt.ireqcntrl.ireq_eof == True));	//Single segment 
	wr_Ftype9_DataStreamingPkt <= FType9_DataStreamingClass  { ftype: wr_InitReqIfcPkt.ireqdata.ireq_ftype,
								cos: {6'h00, wr_InitReqIfcPkt.ireqdata.ireq_prio},
								start: 1'b1,
								ends: 1'b1,
								rsv: 3'b000,
								xheader : 1'b0,
								odd:(rg_Byte_count%4==1 || rg_Byte_count%4==2)?1'b1:1'b0,
								pad:(rg_Byte_count%2==1)?1'b1:1'b0,
								srl: {8'h00, wr_InitReqIfcPkt.ireqdata.ireq_tid},
								data: wr_InitReqIfcPkt.ireqdata.ireq_data};
	rg_validate <= True ;
//	$display("Inside Single segment...%h ,bool:  %h", wr_InitReqIfcPkt.ireqdata.ireq_data,rg_validate);
endrule
rule rl_Ftype9DatastreamingClass_1((wr_InitReqIfcPkt.ireqcntrl.ireq_sof == True)&&(wr_InitReqIfcPkt.ireqcntrl.ireq_vld==True) && (wr_InitReqIfcPkt.ireqdata.ireq_ftype == `RIO_FTYPE9_DATASTREAM) && (wr_InitReqIfcPkt.ireqdata.ireq_byte_count > 8) && (wr_InitReqIfcPkt.ireqcntrl.ireq_eof == False)); //start segment
	wr_Ftype9_DataStreamingPkt <= FType9_DataStreamingClass  { ftype: wr_InitReqIfcPkt.ireqdata.ireq_ftype,
								cos: {6'h00, wr_InitReqIfcPkt.ireqdata.ireq_prio},
								start: 1'b1,
								ends: 1'b0,
								rsv: 3'b000,
								xheader : 1'b0,
								odd: 1'b0,
								pad: 1'b0,
								srl:{8'h00, wr_InitReqIfcPkt.ireqdata.ireq_tid},
								data: wr_InitReqIfcPkt.ireqdata.ireq_data};
	//$display("Inside Continuous(start) segment... : %h bool: %h , %h , vld: %h ", wr_InitReqIfcPkt.ireqdata.ireq_data, wr_InitReqIfcPkt.ireqcntrl.ireq_sof, wr_InitReqIfcPkt.ireqcntrl.ireq_eof, wr_InitReqIfcPkt.ireqcntrl.ireq_vld);
endrule
rule rl_Ftype9DatastreamingClass_2((wr_InitReqIfcPkt.ireqcntrl.ireq_eof == False) &&(wr_InitReqIfcPkt.ireqcntrl.ireq_vld==True) && (wr_InitReqIfcPkt.ireqcntrl.ireq_sof == False) && (rg_Ftype == `RIO_FTYPE9_DATASTREAM) && (rg_Byte_count > 8)) ;				//Continuation segment
	wr_Ftype9_DataStreamingPkt <= FType9_DataStreamingClass  { ftype: rg_Ftype,
								cos: {6'h00, wr_InitReqIfcPkt.ireqdata.ireq_prio},
								start: 1'b0,
								ends: 1'b0,
								rsv: 3'b000,
								xheader : 1'b0,
								odd: 1'b0,
								pad: 1'b0,
								srl: 16'h0000,
								data: wr_InitReqIfcPkt.ireqdata.ireq_data};
//	$display("Inside Continuous(cont) segment... : %h bool: %h , %h , vld: %h ", wr_InitReqIfcPkt.ireqdata.ireq_data, wr_InitReqIfcPkt.ireqcntrl.ireq_sof, wr_InitReqIfcPkt.ireqcntrl.ireq_eof, wr_InitReqIfcPkt.ireqcntrl.ireq_vld);
endrule
rule rl_Ftype9DatastreamingClass_3((wr_InitReqIfcPkt.ireqcntrl.ireq_sof == False) && (wr_InitReqIfcPkt.ireqcntrl.ireq_eof == True) &&(wr_InitReqIfcPkt.ireqcntrl.ireq_vld==True) && (rg_Ftype == `RIO_FTYPE9_DATASTREAM) && (rg_Byte_count > 8)); 
	//End segment
	wr_Ftype9_DataStreamingPkt <= FType9_DataStreamingClass  { ftype: rg_Ftype,
								cos: {6'h00, wr_InitReqIfcPkt.ireqdata.ireq_prio},
								start: 1'b0,
								ends: 1'b1,
								rsv: 3'b000,
								xheader : 1'b0,
								odd:(rg_Byte_count%4==1 || rg_Byte_count%4==2)?1'b1:1'b0,
								pad:(rg_Byte_count%4==1 || rg_Byte_count%4==3)?1'b1:1'b0,
								srl : {7'h00, rg_Byte_count},
								data: wr_InitReqIfcPkt.ireqdata.ireq_data};
//	$display("Inside end segment... : %h bool: %h , %h , vld: %h ", wr_InitReqIfcPkt.ireqdata.ireq_data, wr_InitReqIfcPkt.ireqcntrl.ireq_sof, wr_InitReqIfcPkt.ireqcntrl.ireq_eof, wr_InitReqIfcPkt.ireqcntrl.ireq_vld);
endrule
//////////////////////////
//ftype9 rule ends here//
////////////////////////

method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt pkt);
	wr_InitReqIfcPkt <= pkt;
	wr_InitReqIfc <= pkt;
    rg_testinp <= pkt;
 endmethod

method FType9_DataStreamingClass outputs_Ftype9_DataStreamingClass_();
	return wr_Ftype9_DataStreamingPkt;
endmethod

 method InitiatorReqIfcPkt outputs_InitReqIfcPkt_ ();
	return wr_InitReqIfc;
 endmethod


endmodule : mkRapidIO_Ftype9_Concatenation

endpackage : RapidIO_Ftype9_Concatenation

