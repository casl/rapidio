/*--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Phy_Buffer_Top Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1.This module is developed for the Physical layer 
-- 2.This module integrates the four priority buffers, serving as the top module.
-- 3.
--
-- To be Done:
-- 1. Dequeing the stored data
-- 2. Retransmission strategy to be fixed
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIOPhy_Buffer_Prio_all;

import DefaultValue ::*;
import DReg :: * ;
import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer00 ::*;
import RapidIOPhy_Buffer01 ::*;
import RapidIOPhy_Buffer10 ::*;
import RapidIOPhy_Buffer11 ::*;
import AckId_Lut::*;
import AckId_generator::*;


interface Ifc_RapidIOPhy_Buffer_Prio_all;
	method Action _tx_sof_n(Bool value);//Signals from LOGICAL LAYER
	method Action _tx_eof_n(Bool value);//Signals from LOGICAL LAYER
	method Action _tx_vld_n(Bool value);//Signals from LOGICAL LAYER
	method Action _tx_data(DataPkt value);//Signals from LOGICAL LAYER
	method Action _tx_rem(Bit#(4) value);//Signals from LOGICAL LAYER
	method Action _tx_crf(Bool value);//Signals from LOGICAL LAYER
	method Action _tx_deq(Bool value);//Signal to  deque when packet Accepted(PA) Control symbol(CS)
	method Action _tx_retransmsn(Bool value);// Signal to enable retransmission when Restart From Retry(RFR) CS 
	method Action _tx_stop_txmsn(Bool value);//Signal to stop transmission during a PacketNotAccepted(PNA)orPacketRetry(PR)CS          
	method Action _ack_id_retrans(Bit#(6) value);//Signal to get the retransmitting ackid(Obtained from PNA or PR CS) 
	method Action _ack_id_deq(Bit#(6) value);//Signal to get the dequeing ackid(Obtained from PA)             
   
	method Bool lnk_tvld_n_();//Signals from BUFFER to PHYSICAL LAYER
	method Bool lnk_tsof_n_();//Signals from BUFFER to PHYSICAL LAYER
	method Bool lnk_teof_n_();//Signals from BUFFER to PHYSICAL LAYER
	method DataPkt lnk_td_();//Signals from BUFFER to PHYSICAL LAYER
	method Bit#(4) lnk_trem_();//Signals from BUFFER to PHYSICAL LAYER
	method Bool lnk_tcrf_();//Signals from BUFFER to PHYSICAL LAYER
	method Bool tx_stopped_txmsn_();//Signal to indicate top module has stopped transmitting, so that RFR can be sent 
/*  
//to view written data to buffer
	method BufferData buf_out_00_();
	method BufferData buf_out_01_();
	method BufferData buf_out_10_();
	method BufferData buf_out_11_(); */
endinterface:Ifc_RapidIOPhy_Buffer_Prio_all



(* synthesize *)
//(* always_enabled *)
//(* always_ready *)




(* descending_urgency = "read_disabl_also, read_en_prio_11, read_en_prio_10, read_en_prio_01, read_en_prio_00" *)
(* descending_urgency = "read_enbl, read_ptr_in" *)
(* descending_urgency = "read_enbl, r1_rd_en_ptr" *)


module mkRapidIOPhy_Buffer_Prio_all(Ifc_RapidIOPhy_Buffer_Prio_all);

Ifc_RapidIOPhy_Buffer00 bufer_00 <- mkRapidIOPhy_Buffer00;//Instantiating the buffer with priority-00
Ifc_RapidIOPhy_Buffer01 bufer_01 <- mkRapidIOPhy_Buffer01;//Instantiating the buffer with priority-01
Ifc_RapidIOPhy_Buffer10 bufer_10 <- mkRapidIOPhy_Buffer10;//Instantiating the buffer with priority-10
Ifc_RapidIOPhy_Buffer11 bufer_11 <- mkRapidIOPhy_Buffer11;//Instantiating the buffer with priority-11

Ifc_AckId_generator ack1 <- mkAckId_generator;// Instantiating AckiD Generator
Ifc_AckId_Lut lut <- mkAckId_Lut;// Instantiating LUT

Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bool) wr_tx_crf <- mkDWire(True);

Wire#(Bit#(4)) wr_read_ptr_00 <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr_01 <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr_10 <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr_11 <- mkDWire(0);

Wire#(Bit#(4)) wr_write_ptr_00 <- mkDWire(0);
Wire#(Bit#(4)) wr_write_ptr_01 <- mkDWire(0);
Wire#(Bit#(4)) wr_write_ptr_10 <- mkDWire(0);
Wire#(Bit#(4)) wr_write_ptr_11 <- mkDWire(0);

Wire#(Bit#(1)) wr_valid_check_out_00 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_check_out_01 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_check_out_10 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_check_out_11 <- mkDWire(0);

Wire#(Bit#(1)) wr_valid_next_check_out_00 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_next_check_out_01 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_next_check_out_10 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_next_check_out_11 <- mkDWire(0);

Wire#(Bit#(4)) wr_read_ptr_from_lut <- mkDWire(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_00 <- mkReg(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_01 <- mkReg(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_10 <- mkReg(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_11 <- mkReg(0);

/*
Wire#(BufferData) buf_out_00 <- mkDWire(defaultValue);
Wire#(BufferData) buf_out_01 <- mkDWire(defaultValue);
Wire#(BufferData) buf_out_10 <- mkDWire(defaultValue);
Wire#(BufferData) buf_out_11 <- mkDWire(defaultValue);*/

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4))  wr_lnk_trem <- mkDWire(0);
Wire#(Bool)  wr_lnk_tcrf <- mkDWire(True);

Wire#(Bool)  wr_txmsn_stoped_out <- mkDWire(False);
Wire#(Bool) wr_stop_txmsn <- mkDWire(False);
Wire#(Bool) wr_deq <- mkDWire(False);
Wire#(Bool) wr_retrans <- mkDWire(False);
Wire#(Bit#(6))  wr_ackid_retrans <- mkDWire(0);
Wire#(Bit#(6))  wr_ackid_deq <- mkDWire(0);
Wire#(Bit#(1)) wr_lut_valid_out <- mkDWire(0);//To check whether there is valid entry in lut
Wire#(Bit#(2)) wr_lut_prio_out <- mkDWire(0);

Reg#(Bit#(6))  rg_ackid_retrans <- mkReg(0);
Reg#(Bool) rg_reload_pointer <- mkDReg(False);
Reg#(Bit#(6)) rg_ackid_deq <- mkReg(0);
Reg#(Bool) rg_deq <- mkDReg(False);
Reg#(Bool) rg_deq_buffer <- mkReg(False);
Reg#(Bit#(2)) rg_prio <- mkReg(0);
Reg#(Bool) rg_stop_txmsn <- mkDReg(False);//Signal to top module that transmission has stopped; Also signal to lut for dequeing ackids till the requested one.

//to identify from which buffer section reading is happening
Reg#(Bool) rg_buffer_00_valid <- mkReg(False);
Reg#(Bool) rg_buffer_01_valid <- mkReg(False);
Reg#(Bool) rg_buffer_10_valid <- mkReg(False);
Reg#(Bool) rg_buffer_11_valid <- mkReg(False);







//Writing to buffer according to priority during first cycle
rule prio_identification(wr_tx_vld_n == False && wr_tx_sof_n == False);
	rg_prio <= wr_tx_data[119:118];
	if(wr_tx_data[119:118] == 2'b00)
		begin
		bufer_00._tx_sof_n(wr_tx_sof_n);
		bufer_00._tx_eof_n(wr_tx_eof_n);
		bufer_00._tx_vld_n(wr_tx_vld_n);
		bufer_00._tx_data(wr_tx_data);
		bufer_00._tx_rem(wr_tx_rem);
		bufer_00._tx_crf(wr_tx_crf);
		end

	else if(wr_tx_data[119:118] == 2'b01)
		begin
		bufer_01._tx_sof_n(wr_tx_sof_n);
		bufer_01._tx_eof_n(wr_tx_eof_n);
		bufer_01._tx_vld_n(wr_tx_vld_n);
		bufer_01._tx_data(wr_tx_data);
		bufer_01._tx_rem(wr_tx_rem);
		bufer_01._tx_crf(wr_tx_crf);
		end

	else if(wr_tx_data[119:118] == 2'b10)
		begin
		bufer_10._tx_sof_n(wr_tx_sof_n);
		bufer_10._tx_eof_n(wr_tx_eof_n);
		bufer_10._tx_vld_n(wr_tx_vld_n);
		bufer_10._tx_data(wr_tx_data);
		bufer_10._tx_rem(wr_tx_rem);
		bufer_10._tx_crf(wr_tx_crf);
		end

	else if(wr_tx_data[119:118] == 2'b11)
		begin
		bufer_11._tx_sof_n(wr_tx_sof_n);
		bufer_11._tx_eof_n(wr_tx_eof_n);
		bufer_11._tx_vld_n(wr_tx_vld_n);
		bufer_11._tx_data(wr_tx_data);
		bufer_11._tx_rem(wr_tx_rem);
		bufer_11._tx_crf(wr_tx_crf);
		end

endrule


//writing continuous data to buffer according to priority
rule writing_according_to_prio(wr_tx_vld_n == False && wr_tx_sof_n == True);

	if(rg_prio == 2'b00)
		begin
		bufer_00._tx_sof_n(wr_tx_sof_n);
		bufer_00._tx_eof_n(wr_tx_eof_n);
		bufer_00._tx_vld_n(wr_tx_vld_n);
		bufer_00._tx_data(wr_tx_data);
		bufer_00._tx_rem(wr_tx_rem);
		bufer_00._tx_crf(wr_tx_crf);
		end

	else if(rg_prio == 2'b01)
		begin
		bufer_01._tx_sof_n(wr_tx_sof_n);
		bufer_01._tx_eof_n(wr_tx_eof_n);
		bufer_01._tx_vld_n(wr_tx_vld_n);
		bufer_01._tx_data(wr_tx_data);
		bufer_01._tx_rem(wr_tx_rem);
		bufer_01._tx_crf(wr_tx_crf);
		end

	else if(rg_prio == 2'b10)
		begin
		bufer_10._tx_sof_n(wr_tx_sof_n);
		bufer_10._tx_eof_n(wr_tx_eof_n);
		bufer_10._tx_vld_n(wr_tx_vld_n);
		bufer_10._tx_data(wr_tx_data);
		bufer_10._tx_rem(wr_tx_rem);
		bufer_10._tx_crf(wr_tx_crf);
		end

	else if(rg_prio == 2'b11)
		begin
		bufer_11._tx_sof_n(wr_tx_sof_n);
		bufer_11._tx_eof_n(wr_tx_eof_n);
		bufer_11._tx_vld_n(wr_tx_vld_n);
		bufer_11._tx_data(wr_tx_data);
		bufer_11._tx_rem(wr_tx_rem);
		bufer_11._tx_crf(wr_tx_crf);
		end

endrule


//Ackid assignment
rule ackid_assign;
	bufer_00._tx_ack(ack1.ack_id_out_());
	bufer_01._tx_ack(ack1.ack_id_out_());
	bufer_10._tx_ack(ack1.ack_id_out_());
	bufer_11._tx_ack(ack1.ack_id_out_());
endrule


rule read_ptr_in;
//Read_pointer location
	wr_read_ptr_00 <= bufer_00.out_rd_ptr_();
	wr_read_ptr_01 <= bufer_01.out_rd_ptr_();
	wr_read_ptr_10 <= bufer_10.out_rd_ptr_();
	wr_read_ptr_11 <= bufer_11.out_rd_ptr_();
//Write pointer location
	wr_write_ptr_00 <= bufer_00.out_wrt_ptr_();
	wr_write_ptr_01 <= bufer_01.out_wrt_ptr_();
	wr_write_ptr_10 <= bufer_10.out_wrt_ptr_();
	wr_write_ptr_11 <= bufer_11.out_wrt_ptr_();
//To check wheteher data is present in the current location of the corresponding buffer
	wr_valid_check_out_00 <= bufer_00.valid_check_out_();
	wr_valid_check_out_01 <= bufer_01.valid_check_out_();
	wr_valid_check_out_10 <= bufer_10.valid_check_out_();
	wr_valid_check_out_11 <= bufer_11.valid_check_out_();
//To check wheteher data is present in the next location of the corresponding buffer
	wr_valid_next_check_out_00 <= bufer_00.valid_next_check_out_();
	wr_valid_next_check_out_01 <= bufer_01.valid_next_check_out_();
	wr_valid_next_check_out_10 <= bufer_10.valid_next_check_out_();
	wr_valid_next_check_out_11 <= bufer_11.valid_next_check_out_();
endrule


rule displ_pointrs;
	$display("write_ptr_00 = %b",wr_write_ptr_00);
	$display("write_ptr_01 = %b",wr_write_ptr_01);
	$display("write_ptr_10 = %b",wr_write_ptr_10);
	$display("write_ptr_11 = %b",wr_write_ptr_11);

	$display("read_ptr_00 = %b",wr_read_ptr_00);
	$display("read_ptr_01 = %b",wr_read_ptr_01);
	$display("read_ptr_10 = %b",wr_read_ptr_10);
	$display("read_ptr_11 = %b",wr_read_ptr_11);

	$display("wr_valid_check_out_00 = %b",wr_valid_check_out_00);
	$display("wr_valid_check_out_01 = %b",wr_valid_check_out_01);
	$display("wr_valid_check_out_10 = %b",wr_valid_check_out_10);
	$display("wr_valid_check_out_11 = %b",wr_valid_check_out_11);

	$display("Ackid - %b",ack1.ack_id_out_());
	$display("rg_reload_pointer = %b",rg_reload_pointer);
endrule


//Initial read enabling by threshold crossing(transmission starts if any one section crosses threshold value)
//Threshold values for Low to high priority is in the order 5,4,3 and 2 respectively
rule r1_rd_en_ptr(((wr_write_ptr_00 >= 5 && wr_read_ptr_00 == 0) || (wr_write_ptr_01 >= 4 && wr_read_ptr_01 == 0) || (wr_write_ptr_10 >= 3 && wr_read_ptr_10 == 0) || (wr_write_ptr_11 >= 2 && wr_read_ptr_11 == 0)) && wr_stop_txmsn != True);

	bufer_00._read_ptr_valid(4'b0001);
	bufer_01._read_ptr_valid(4'b0001);
	bufer_10._read_ptr_valid(4'b0001);
	bufer_11._read_ptr_valid(4'b0001);

	if(wr_write_ptr_00 >= 5 && wr_read_ptr_00 == 0)
		begin	
		bufer_00._tx_read_en(True);
		rg_buffer_00_valid <= True;
		end
	else if(wr_write_ptr_01 >= 4 && wr_read_ptr_01 == 0)
		begin
		bufer_01._tx_read_en(True);
		rg_buffer_01_valid <= True;
		end	
	else if(wr_write_ptr_10 >= 3 && wr_read_ptr_10 == 0)
		begin
		bufer_10._tx_read_en(True);
		rg_buffer_10_valid <= True;
		end		
	else if(wr_write_ptr_11 >= 2 && wr_read_ptr_11 == 0)
		begin
		bufer_11._tx_read_en(True);
		rg_buffer_11_valid <= True;
		end		
endrule


//Transmission with respect to priority (enabled-descending urgency attribute for the corresponding rules)
rule read_en_prio_00((wr_lnk_teof_n == False && ((rg_buffer_00_valid == True && wr_valid_next_check_out_00 == 1'b1) || (rg_buffer_00_valid == False && wr_valid_check_out_00 == 1'b1)) && wr_stop_txmsn != True) || (wr_retrans == True && wr_stop_txmsn != True));

	bufer_00._tx_read_en(True);
	bufer_01._tx_read_en(False);
	bufer_10._tx_read_en(False);
	bufer_11._tx_read_en(False);

	rg_buffer_00_valid <= True;
	rg_buffer_01_valid <= False;
	rg_buffer_10_valid <= False;
	rg_buffer_11_valid <= False; 
endrule


rule read_en_prio_01((wr_lnk_teof_n == False && ((rg_buffer_01_valid == True && wr_valid_next_check_out_01 == 1'b1) || (rg_buffer_01_valid == False && wr_valid_check_out_01 == 1'b1)) && wr_stop_txmsn != True) || (wr_retrans == True && wr_stop_txmsn != True));
	bufer_01._tx_read_en(True);
	bufer_00._tx_read_en(False);
	bufer_10._tx_read_en(False);
	bufer_11._tx_read_en(False);

	rg_buffer_00_valid <= False;
	rg_buffer_01_valid <= True;
	rg_buffer_10_valid <= False;
	rg_buffer_11_valid <= False;
endrule


rule read_en_prio_10((wr_lnk_teof_n == False && ((rg_buffer_10_valid == True && wr_valid_next_check_out_10 == 1'b1) || (rg_buffer_10_valid == False && wr_valid_check_out_10 == 1'b1)) && wr_stop_txmsn != True) || (wr_retrans == True && wr_stop_txmsn != True));
	bufer_10._tx_read_en(True);
	bufer_00._tx_read_en(False);
	bufer_01._tx_read_en(False);
	bufer_11._tx_read_en(False);

	rg_buffer_00_valid <= False;
	rg_buffer_01_valid <= False;
	rg_buffer_10_valid <= True;
	rg_buffer_11_valid <= False; 
endrule


rule read_en_prio_11((wr_lnk_teof_n == False && ((rg_buffer_11_valid == True && wr_valid_next_check_out_11 == 1'b1) || (rg_buffer_11_valid == False && wr_valid_check_out_11 == 1'b1)) && wr_stop_txmsn != True) || (wr_retrans == True && wr_stop_txmsn != True));
	bufer_11._tx_read_en(True);
	bufer_00._tx_read_en(False);
	bufer_01._tx_read_en(False);
	bufer_10._tx_read_en(False);

	rg_buffer_00_valid <= False;
	rg_buffer_01_valid <= False;
	rg_buffer_10_valid <= False;
	rg_buffer_11_valid <= True;
endrule


//sof_n given to ackid generator to increment
rule ack_incrmnt(wr_lnk_tsof_n == False);
	ack1._ack_en(False);
endrule

//storing to lut while transmitting (valid bit - ackid - prio section - pointer value)
rule lut_store(wr_stop_txmsn != True  && wr_lnk_tsof_n == False);
		lut._store(True);
		lut._ackid_store(ack1.ack_id_out_());
		if(rg_buffer_11_valid == True)
			begin
			lut._rd_ptr_in(wr_read_ptr_11);
			lut._prio_in(2'b11);
			end
		else if(rg_buffer_10_valid == True)
			begin
			lut._rd_ptr_in(wr_read_ptr_10);
			lut._prio_in(2'b10);
			end
		else if(rg_buffer_01_valid == True)
			begin
			lut._rd_ptr_in(wr_read_ptr_01);
			lut._prio_in(2'b01);
			end
		else if(rg_buffer_00_valid == True)
			begin
			lut._rd_ptr_in(wr_read_ptr_00);
			lut._prio_in(2'b00);
			end
endrule

//During PNA or PR
rule read_disabl(wr_stop_txmsn == True);
	ack1._ack_in(wr_ackid_retrans);//retried ackid given to ackid generator
	ack1._retrans(wr_stop_txmsn);//Signal to ackid generator in case of stopping transmission and then retransmission
//reloading read and write count values in basic register
	bufer_11._tx_stop_txmsn(wr_stop_txmsn);
	bufer_10._tx_stop_txmsn(wr_stop_txmsn);
	bufer_01._tx_stop_txmsn(wr_stop_txmsn);
	bufer_00._tx_stop_txmsn(wr_stop_txmsn);
//Signal to indicate reading has been disabled

	rg_buffer_00_valid <= False;
	rg_buffer_01_valid <= False;
	rg_buffer_10_valid <= False;
	rg_buffer_11_valid <= False;
	

	$display("Error reported:Ackid to be retransmitted - %b",wr_ackid_retrans);
endrule


rule read_disabl_also(wr_stop_txmsn == True);
//Disabling buffer sections
	bufer_11._tx_read_en(!(wr_stop_txmsn));
	bufer_10._tx_read_en(!(wr_stop_txmsn));
	bufer_01._tx_read_en(!(wr_stop_txmsn));
	bufer_00._tx_read_en(!(wr_stop_txmsn));
endrule


//reloading read pointer in each buffer sections
rule lut_retrans_reload(rg_reload_pointer);
	lut._stop_txmsn(True);//should be given after clearing lut
	//lut._ackid_retrans(rg_ackid_retrans);
endrule



//providing read pointer in every section for retransmission
rule read_ptr_reload(wr_retrans == True);
	lut._retrans(wr_retrans);
	if(lut.out_00_enter_())
		bufer_00._read_ptr_valid(lut.out_00_());
	else
		bufer_00._read_ptr_valid(wr_read_ptr_00);
	
	if(lut.out_01_enter_())
		bufer_01._read_ptr_valid(lut.out_01_());
	else
		bufer_01._read_ptr_valid(wr_read_ptr_01);

	if(lut.out_10_enter_())
		bufer_10._read_ptr_valid(lut.out_10_());
	else
		bufer_10._read_ptr_valid(wr_read_ptr_10);

	if(lut.out_11_enter_())
		bufer_11._read_ptr_valid(lut.out_11_());
	else
		bufer_11._read_ptr_valid(wr_read_ptr_11);
endrule




//Restart from Retry
rule read_enbl(wr_retrans == True);
	$display("Retransmit retried ackid");
	bufer_00._tx_retransmsn(wr_retrans);
	bufer_01._tx_retransmsn(wr_retrans);
	bufer_10._tx_retransmsn(wr_retrans);
	bufer_11._tx_retransmsn(wr_retrans);
endrule

//------------------------------------------------------dequeuing---------------------------------------------------------//
//Copying ackids to be dequeued from original lut to a similar lut both in case of retransmission and dequeuing.
rule lut_copy_deq(wr_deq == True ||  wr_stop_txmsn == True);
	lut._copy(True);
	if(wr_deq)//PA
	begin
		$display("Packet Accepted:Ackid to be dequeued - %b",wr_ackid_deq);
		lut._ackid_copy(wr_ackid_deq);//Dequeing including the ackid
		rg_ackid_deq <= wr_ackid_deq;
		rg_deq <= True;//Signal to deq from original lut 
	end
	else//PNA
	begin
		rg_ackid_retrans <= wr_ackid_retrans - 1;
		rg_stop_txmsn <= True;Signal to deq from original lut
		lut._ackid_copy(wr_ackid_retrans - 1);//Dequeuing excluding the ackid.
	end
endrule


//Clearing entries to be dequeued from original lut
rule lut_clear(rg_deq || rg_stop_txmsn);
	rg_deq_buffer <= True;
	lut._clear(True);
	if(rg_deq)//PA
		begin
		lut._ackid_clear(rg_ackid_deq);
		end
	else
		begin
		rg_reload_pointer <= True;
		lut._ackid_clear(rg_ackid_retrans);
		end
endrule

//Identifying location in buffer to be dequeued
rule deq_location_pointer(rg_deq_buffer);
lut._lut_access_out(True);
lut._ackid_access(rg_ackid_deq);
endrule


//Dequeuing packets from buffer
rule buffer_location_deq(rg_deq_buffer);
if(lut.prio_out_() == 2'b00)
			begin
$display("deq from buffer_00");
			bufer_00._tx_deq(True);
			bufer_00._pointer_deq(lut.read_pointer_out_());
			end
	else if(lut.prio_out_() == 2'b01)
			begin
$display("deq from buffer_01");
			bufer_01._tx_deq(True);
			bufer_01._pointer_deq(lut.read_pointer_out_());
			end
	else if(lut.prio_out_() == 2'b10)
			begin
$display("deq from buffer_10");
			bufer_10._tx_deq(True);
			bufer_10._pointer_deq(lut.read_pointer_out_());
			end
	else if(lut.prio_out_() == 2'b11)
			begin
$display("deq from buffer_11");
			bufer_11._tx_deq(True);
			bufer_11._pointer_deq(lut.read_pointer_out_());
			end

rg_ackid_deq <= rg_ackid_deq - 1;//dequeing all packets till the accepted ackid
if(rg_ackid_deq == lut.last_deq_ackid_())//disabling dequeing
rg_deq_buffer <= False;
endrule


//outputs from buffer
rule buffer_outs;
	wr_lnk_tvld_n <= bufer_00.lnk_tvld_n_() && bufer_01.lnk_tvld_n_() && bufer_10.lnk_tvld_n_() && bufer_11.lnk_tvld_n_(); 
	wr_lnk_tsof_n <= bufer_00.lnk_tsof_n_() && bufer_01.lnk_tsof_n_() && bufer_10.lnk_tsof_n_() && bufer_11.lnk_tsof_n_();
	wr_lnk_teof_n <= bufer_00.lnk_teof_n_() && bufer_01.lnk_teof_n_() && bufer_10.lnk_teof_n_() && bufer_11.lnk_teof_n_();
	wr_lnk_td <= bufer_00.lnk_td_() | bufer_01.lnk_td_() | bufer_10.lnk_td_() | bufer_11.lnk_td_();
	wr_lnk_trem <= bufer_00.lnk_trem_() | bufer_01.lnk_trem_() | bufer_10.lnk_trem_() | bufer_11.lnk_trem_();
	wr_lnk_tcrf <= bufer_00.lnk_tcrf_() && bufer_01.lnk_tcrf_() && bufer_10.lnk_tcrf_() && bufer_11.lnk_tcrf_();
/*
	buf_out_00 <= bufer_00.buf_out_();
	buf_out_01 <= bufer_01.buf_out_();
	buf_out_10 <= bufer_10.buf_out_();
	buf_out_11 <= bufer_11.buf_out_(); */
endrule



//Input methods
method Action _tx_sof_n(Bool value);
	wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
	wr_tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
	wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
	wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
	wr_tx_rem <= value;
endmethod

method Action _tx_crf(Bool value);
	wr_tx_crf <= value;
endmethod

method Action _tx_retransmsn(Bool value);
	wr_retrans <= value;
endmethod

method Action _tx_stop_txmsn(Bool value);
	wr_stop_txmsn <= value;
endmethod

method Action _ack_id_retrans(Bit#(6) value);
	wr_ackid_retrans <= value;
endmethod

method Action _tx_deq(Bool value);
	wr_deq <= value;
endmethod

method Action _ack_id_deq(Bit#(6) value); 
	wr_ackid_deq <= value;
endmethod	



//Output methods
method Bool tx_stopped_txmsn_();
	return rg_stop_txmsn;
endmethod

method Bool lnk_tvld_n_();
	return wr_lnk_tvld_n;
endmethod

method Bool lnk_tsof_n_();
	return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
	return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
	return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
	return  wr_lnk_trem;
endmethod

method Bool lnk_tcrf_();
	return  wr_lnk_tcrf;
endmethod
/*
method BufferData buf_out_00_();
     return buf_out_00;
endmethod

method BufferData buf_out_01_();
     return buf_out_01;
endmethod

method BufferData buf_out_10_();
     return buf_out_10;
endmethod

method BufferData buf_out_11_();
     return buf_out_11;
endmethod
*/

endmodule:mkRapidIOPhy_Buffer_Prio_all
endpackage:RapidIOPhy_Buffer_Prio_all

