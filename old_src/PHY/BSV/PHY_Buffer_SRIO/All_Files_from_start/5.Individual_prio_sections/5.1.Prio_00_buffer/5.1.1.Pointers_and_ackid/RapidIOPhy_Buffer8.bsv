package RapidIOPhy_Buffer8;

import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer5 ::*;
//import AckId_Lut::*;


interface Ifc_RapidIOPhy_Buffer8;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   //method Action _tx_rdy_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf(Bit#(2) value);
   method Action _tx_read(Bit#(1) value);
   //method Action _tx_rg12(Bit#(4) value);
   method Action _tx_deq(Bit#(1) value);
   //method Action _ack_id(Bit#(2) value);
   method Action _tx_ack(Bit#(4) value);
   method Action _ack_id(Bit#(4) value);
   
   method RegBuf buf_out_();
   method Bool lnk_tvld_n_();
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bit#(4) lnk_trem_();
   method Bit#(2) lnk_tcrf_();
   

endinterface:Ifc_RapidIOPhy_Buffer8



(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer8(Ifc_RapidIOPhy_Buffer8);

//Ifc_AckId_Lut a2 <- mkAckId_Lut;
Ifc_RapidIOPhy_Buffer5 r1 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r2 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r3 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r4 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r5 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r6 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r7 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r8 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r9 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r10 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r11 <- mkRapidIOPhy_Buffer5;




Wire#(Bool) tx_sof_n <- mkDWire(True);
Wire#(Bool) tx_eof_n <- mkDWire(True);
Wire#(Bool) tx_vld_n <- mkDWire(True);
//Wire#(Bool) tx_rdy_n <- mkDWire(True);
Wire#(DataPkt) tx_data <- mkDWire(0);
Wire#(Bit#(4)) tx_rem <- mkDWire(0);
Wire#(Bit#(2)) tx_crf <- mkDWire(0);

Wire#(Bool) lnk_tvld_n <- mkDWire(True);
Wire#(Bool) lnk_tsof_n <- mkDWire(True);
Wire#(Bool) lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) lnk_td <- mkDWire(0);
Wire#(Bit#(4)) lnk_trem <- mkDWire(0);
Wire#(Bit#(2)) lnk_tcrf <- mkDWire(0);

Reg#(Bit#(4)) rg_wrt_ptr <- mkReg(4'b0001);
Reg#(Bit#(1)) rg_read <- mkReg(0);
Wire#(Bit#(1)) wr_deq <- mkDWire(0);
Wire#(RegBuf) buf_out <- mkDWire(0);
Wire#(Bit#(4)) tx_ack <- mkDWire(0);
Wire#(Bit#(4)) ackid <- mkDWire(0);
Wire#(Bit#(4)) wr_12 <- mkDWire(0);

rule r1_wr_ptr;

wr_12 <= rg_wrt_ptr;
$display("wrt_ptr =%b",rg_wrt_ptr);

if(tx_eof_n == False)
begin
if(rg_wrt_ptr == 4'b1011) 
rg_wrt_ptr <= 4'b0001;
else
rg_wrt_ptr <= rg_wrt_ptr + 1;
end

endrule


rule r1_vld;

$display("wr_12 = %b",wr_12);
if(wr_12 == 4'b0001)
r1._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b0010)
r2._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b0011)
r3._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b0100)
r4._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b0101)
r5._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b0110)
r6._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b0111)
r7._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b1000)
r8._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b1001)
r9._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b1010)
r10._tx_vld_n(tx_vld_n);
else if(wr_12 == 4'b1011)
r11._tx_vld_n(tx_vld_n);

endrule

rule r1_sof;

if(wr_12 == 4'b0001)
r1._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b0010)
r2._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b0011)
r3._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b0100)
r4._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b0101)
r5._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b0110)
r6._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b0111)
r7._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b1000)
r8._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b1001)
r9._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b1010)
r10._tx_sof_n(tx_sof_n);
else if(wr_12 == 4'b1011)
r11._tx_sof_n(tx_sof_n);

endrule

rule r1_eof;

if(wr_12 == 4'b0001)
r1._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b0010)
r2._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b0011)
r3._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b0100)
r4._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b0101)
r5._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b0110)
r6._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b0111)
r7._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b1000)
r8._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b1001)
r9._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b1010)
r10._tx_eof_n(tx_eof_n);
else if(wr_12 == 4'b1011)
r11._tx_eof_n(tx_eof_n);

endrule



rule r1_data;

if(wr_12 == 4'b0001)
r1._tx_data(tx_data);
else if(wr_12 == 4'b0010)
r2._tx_data(tx_data);
else if(wr_12 == 4'b0011)
r3._tx_data(tx_data);
else if(wr_12 == 4'b0100)
r4._tx_data(tx_data);
else if(wr_12 == 4'b0101)
r5._tx_data(tx_data);
else if(wr_12 == 4'b0110)
r6._tx_data(tx_data);
else if(wr_12 == 4'b0111)
r7._tx_data(tx_data);
else if(wr_12 == 4'b1000)
r8._tx_data(tx_data);
else if(wr_12 == 4'b1001)
r9._tx_data(tx_data);
else if(wr_12 == 4'b1010)
r10._tx_data(tx_data);
else if(wr_12 == 4'b1011)
r11._tx_data(tx_data);

endrule

rule r1_rem;

if(wr_12 == 4'b0001)
r1._tx_rem(tx_rem);
else if(wr_12 == 4'b0010)
r2._tx_rem(tx_rem);
else if(wr_12 == 4'b0011)
r3._tx_rem(tx_rem);
else if(wr_12 == 4'b0100)
r4._tx_rem(tx_rem);
else if(wr_12 == 4'b0101)
r5._tx_rem(tx_rem);
else if(wr_12 == 4'b0110)
r6._tx_rem(tx_rem);
else if(wr_12 == 4'b0111)
r7._tx_rem(tx_rem);
else if(wr_12 == 4'b1000)
r8._tx_rem(tx_rem);
else if(wr_12 == 4'b1001)
r9._tx_rem(tx_rem);
else if(wr_12 == 4'b1010)
r10._tx_rem(tx_rem);
else if(wr_12 == 4'b1011)
r11._tx_rem(tx_rem);

endrule

rule r1_crf;

if(wr_12 == 4'b0001)
r1._tx_crf(tx_crf);
else if(wr_12 == 4'b0010)
r2._tx_crf(tx_crf);
else if(wr_12 == 4'b0011)
r3._tx_crf(tx_crf);
else if(wr_12 == 4'b0100)
r4._tx_crf(tx_crf);
else if(wr_12 == 4'b0101)
r5._tx_crf(tx_crf);
else if(wr_12 == 4'b0110)
r6._tx_crf(tx_crf);
else if(wr_12 == 4'b0111)
r7._tx_crf(tx_crf);
else if(wr_12 == 4'b1000)
r8._tx_crf(tx_crf);
else if(wr_12 == 4'b1001)
r9._tx_crf(tx_crf);
else if(wr_12 == 4'b1010)
r10._tx_crf(tx_crf);
else if(wr_12 == 4'b1011)
r11._tx_crf(tx_crf);

endrule

rule r1_ack(rg_read == 1);

if(wr_12 == 4'b0001)
r1._tx_ack(tx_ack);
else if(wr_12 == 4'b0010)
r2._tx_ack(tx_ack);
else if(wr_12 == 4'b0011)
r3._tx_ack(tx_ack);
else if(wr_12 == 4'b0100)
r4._tx_ack(tx_ack);
else if(wr_12 == 4'b0101)
r5._tx_ack(tx_ack);
else if(wr_12 == 4'b0110)
r6._tx_ack(tx_ack);
else if(wr_12 == 4'b0111)
r7._tx_ack(tx_ack);
else if(wr_12 == 4'b1000)
r8._tx_ack(tx_ack);
else if(wr_12 == 4'b1001)
r9._tx_ack(tx_ack);
else if(wr_12 == 4'b1010)
r10._tx_ack(tx_ack);
//$display("tx_ack == %b",tx_ack);
else if(wr_12 == 4'b1011)
r11._tx_ack(tx_ack);

endrule

rule r1_read(rg_read == 1);

if(wr_12 == 4'b0001)
r1._tx_read(rg_read);
else if(wr_12 == 4'b0010)
r2._tx_read(rg_read);
else if(wr_12 == 4'b0011)
r3._tx_read(rg_read);
else if(wr_12 == 4'b0100)
r4._tx_read(rg_read);
else if(wr_12 == 4'b0101)
r5._tx_read(rg_read);
else if(wr_12 == 4'b0110)
r6._tx_read(rg_read);
else if(wr_12 == 4'b0111)
r7._tx_read(rg_read);
else if(wr_12 == 4'b1000)
r8._tx_read(rg_read);
else if(wr_12 == 4'b1001)
r9._tx_read(rg_read);
else if(wr_12 == 4'b1010)
r10._tx_read(rg_read);
else if(wr_12 == 4'b1011)
r11._tx_read(rg_read);

endrule

/*rule r1_read1(rg_read == 1);
if(wr_12 == 2'b10)
begin
r1._rd_ptr_in(wr_12);

//r1._tx_read(rg_read);
end
else if(wr_12 == 2'b11)
begin
r2._rd_ptr_in(wr_12);

//r2._tx_read(rg_read);
end
endrule*/

rule r1_deq(wr_deq == 1);//when rg_buf[ackid]==lnk_last_ack,deq = 1
//a2._identify(2'b10);
//a2._ackid(ackid);
//$display("fire");
if(ackid == 4'b0000)
r1._tx_deq(wr_deq);
else if(ackid == 4'b0001)
r2._tx_deq(wr_deq);
else if(ackid == 4'b0010)
r3._tx_deq(wr_deq);
else if(ackid == 4'b0011)
r4._tx_deq(wr_deq);
else if(ackid == 4'b0100)
r5._tx_deq(wr_deq);
else if(ackid == 4'b0101)
r6._tx_deq(wr_deq);
else if(ackid == 4'b0110)
r7._tx_deq(wr_deq);
else if(ackid == 4'b0111)
r8._tx_deq(wr_deq);
else if(ackid == 4'b1000)
r9._tx_deq(wr_deq);
else if(ackid == 4'b1001)
r10._tx_deq(wr_deq);
else if(ackid == 4'b1010)
r11._tx_deq(wr_deq);

endrule

rule r1_lnk_vld(rg_read == 1);

if(wr_12 == 4'b0001)
lnk_tvld_n <= r1.lnk_tvld_n_();
else if(wr_12 == 4'b0010)
lnk_tvld_n <= r2.lnk_tvld_n_();
else if(wr_12 == 4'b0011)
lnk_tvld_n <= r3.lnk_tvld_n_();
else if(wr_12 == 4'b0100)
lnk_tvld_n <= r4.lnk_tvld_n_();
else if(wr_12 == 4'b0101)
lnk_tvld_n <= r5.lnk_tvld_n_();
else if(wr_12 == 4'b0110)
lnk_tvld_n <= r6.lnk_tvld_n_();
else if(wr_12 == 4'b0111)
lnk_tvld_n <= r7.lnk_tvld_n_();
else if(wr_12 == 4'b1000)
lnk_tvld_n <= r8.lnk_tvld_n_();
else if(wr_12 == 4'b1001)
lnk_tvld_n <= r9.lnk_tvld_n_();
else if(wr_12 == 4'b1010)
lnk_tvld_n <= r10.lnk_tvld_n_();
else if(wr_12 == 4'b1011)
lnk_tvld_n <= r11.lnk_tvld_n_();

endrule

rule r1_lnk_sof(rg_read == 1);

if(wr_12 == 4'b0001)
lnk_tsof_n <= r1.lnk_tsof_n_();
else if(wr_12 == 4'b0010)
lnk_tsof_n <= r2.lnk_tsof_n_();
else if(wr_12 == 4'b0011)
lnk_tsof_n <= r3.lnk_tsof_n_();
else if(wr_12 == 4'b0100)
lnk_tsof_n <= r4.lnk_tsof_n_();
else if(wr_12 == 4'b0101)
lnk_tsof_n <= r5.lnk_tsof_n_();
else if(wr_12 == 4'b0110)
lnk_tsof_n <= r6.lnk_tsof_n_();
else if(wr_12 == 4'b0111)
lnk_tsof_n <= r7.lnk_tsof_n_();
else if(wr_12 == 4'b1000)
lnk_tsof_n <= r8.lnk_tsof_n_();
else if(wr_12 == 4'b1001)
lnk_tsof_n <= r9.lnk_tsof_n_();
else if(wr_12 == 4'b1010)
lnk_tsof_n <= r10.lnk_tsof_n_();
else if(wr_12 == 4'b1011)
lnk_tsof_n <= r11.lnk_tsof_n_();

endrule

rule r1_lnk_eof(rg_read == 1);

if(wr_12 == 4'b0001)
lnk_teof_n <= r1.lnk_teof_n_();
else if(wr_12 == 4'b0010)
lnk_teof_n <= r2.lnk_teof_n_();
else if(wr_12 == 4'b0011)
lnk_teof_n <= r3.lnk_teof_n_();
else if(wr_12 == 4'b0100)
lnk_teof_n <= r4.lnk_teof_n_();
else if(wr_12 == 4'b0101)
lnk_teof_n <= r5.lnk_teof_n_();
else if(wr_12 == 4'b0110)
lnk_teof_n <= r6.lnk_teof_n_();
else if(wr_12 == 4'b0111)
lnk_teof_n <= r7.lnk_teof_n_();
else if(wr_12 == 4'b1000)
lnk_teof_n <= r8.lnk_teof_n_();
else if(wr_12 == 4'b1001)
lnk_teof_n <= r9.lnk_teof_n_();
else if(wr_12 == 4'b1010)
lnk_teof_n <= r10.lnk_teof_n_();
else if(wr_12 == 4'b1011)
lnk_teof_n <= r11.lnk_teof_n_();

endrule

rule r1_lnk_data(rg_read == 1);

if(wr_12 == 4'b0001)
lnk_td <= r1.lnk_td_();
else if(wr_12 == 4'b0010)
lnk_td <= r2.lnk_td_();
else if(wr_12 == 4'b0011)
lnk_td <= r3.lnk_td_();
else if(wr_12 == 4'b0100)
lnk_td <= r4.lnk_td_();
else if(wr_12 == 4'b0101)
lnk_td <= r5.lnk_td_();
else if(wr_12 == 4'b0110)
lnk_td <= r6.lnk_td_();
else if(wr_12 == 4'b0111)
lnk_td <= r7.lnk_td_();
else if(wr_12 == 4'b1000)
lnk_td <= r8.lnk_td_();
else if(wr_12 == 4'b1001)
lnk_td <= r9.lnk_td_();
else if(wr_12 == 4'b1010)
lnk_td <= r10.lnk_td_();
else if(wr_12 == 4'b1011)
lnk_td <= r11.lnk_td_();

endrule

rule r1_lnk_rem(rg_read == 1);

if(wr_12 == 4'b0001)
lnk_trem <= r1.lnk_trem_();
else if(wr_12 == 4'b0010)
lnk_trem <= r2.lnk_trem_();
else if(wr_12 == 4'b0011)
lnk_trem <= r3.lnk_trem_();
else if(wr_12 == 4'b0100)
lnk_trem <= r4.lnk_trem_();
else if(wr_12 == 4'b0101)
lnk_trem <= r5.lnk_trem_();
else if(wr_12 == 4'b0110)
lnk_trem <= r6.lnk_trem_();
else if(wr_12 == 4'b0111)
lnk_trem <= r7.lnk_trem_();
else if(wr_12 == 4'b1000)
lnk_trem <= r8.lnk_trem_();
else if(wr_12 == 4'b1001)
lnk_trem <= r9.lnk_trem_();
else if(wr_12 == 4'b1010)
lnk_trem <= r10.lnk_trem_();
else if(wr_12 == 4'b1011)
lnk_trem <= r11.lnk_trem_();

endrule

rule r1_lnk_crf(rg_read == 1);

if(wr_12 == 4'b0001)
lnk_tcrf <= r1.lnk_tcrf_();
else if(wr_12 == 4'b0010)
lnk_tcrf <= r2.lnk_tcrf_();
else if(wr_12 == 4'b0011)
lnk_tcrf <= r3.lnk_tcrf_();
else if(wr_12 == 4'b0100)
lnk_tcrf <= r4.lnk_tcrf_();
else if(wr_12 == 4'b0101)
lnk_tcrf <= r5.lnk_tcrf_();
else if(wr_12 == 4'b0110)
lnk_tcrf <= r6.lnk_tcrf_();
else if(wr_12 == 4'b0111)
lnk_tcrf <= r7.lnk_tcrf_();
else if(wr_12 == 4'b1000)
lnk_tcrf <= r8.lnk_tcrf_();
else if(wr_12 == 4'b1001)
lnk_tcrf <= r9.lnk_tcrf_();
else if(wr_12 == 4'b1010)
lnk_tcrf <= r10.lnk_tcrf_();
else if(wr_12 == 4'b1011)
lnk_tcrf <= r11.lnk_tcrf_();

endrule

rule r1_lnk_out;

if(wr_12 == 4'b0001)
//begin
buf_out <= r1.buf_out_();
//$display("Data stored in register is %h",buf_out);
//end
else if(wr_12 == 4'b0010)
buf_out <= r2.buf_out_();
else if(wr_12 == 4'b0011)
buf_out <= r3.buf_out_();
else if(wr_12 == 4'b0100)
buf_out <= r4.buf_out_();
else if(wr_12 == 4'b0101)
buf_out <= r5.buf_out_();
else if(wr_12 == 4'b0110)
buf_out <= r6.buf_out_();
else if(wr_12 == 4'b0111)
buf_out <= r7.buf_out_();
else if(wr_12 == 4'b1000)
buf_out <= r8.buf_out_();
else if(wr_12 == 4'b1001)
buf_out <= r9.buf_out_();
else if(wr_12 == 4'b1010)
buf_out <= r10.buf_out_();
else if(wr_12 == 4'b1011)
buf_out <= r11.buf_out_();

endrule















method Action _tx_sof_n(Bool value);
     tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
     tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     tx_rem <= value;
endmethod

method Action _tx_crf(Bit#(2) value);
     tx_crf <= value;
endmethod

method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod

//method Action _tx_rg12(Bit#(4) value);
     //wr_12 <= value;
//endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

method Action _tx_ack(Bit#(4) value);
	tx_ack <= value;
endmethod


method Action _ack_id(Bit#(4) value);
     ackid <= value;
endmethod

method Bool lnk_tvld_n_();
     return lnk_tvld_n;
endmethod

method RegBuf buf_out_();
     return buf_out;
endmethod

method Bool lnk_tsof_n_();
     return lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return lnk_trem;
endmethod

method Bit#(2) lnk_tcrf_();
      return lnk_tcrf;
endmethod

endmodule:mkRapidIOPhy_Buffer8
endpackage:RapidIOPhy_Buffer8
