package Tb_RapidIOPhy_Buffer5;

import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer5 ::*;

(*synthesize*)
//(*always_enabled*)
//(*always_ready*)

module mkTb_RapidIOPhy_Buffer5(Empty);


Ifc_RapidIOPhy_Buffer5 bfr5 <- mkRapidIOPhy_Buffer5;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);
//Reg#(Bool) sof <- mkReg(True);
//Reg#(Bool) eof <- mkReg(True);

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 52 )
		$finish (0);
endrule 


/*rule r1(reg_ref_clk == 5);
bfr5._tx_sof_n(False);
bfr5._tx_eof_n(False);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'hCEA0F52AD3E66EFC11375A8D9C8EFC0A);
bfr5._tx_rem(4'b111);
bfr5._tx_crf(2'b11);
//$display("Regstr value is %h",bfr5.buf_out_());
endrule*/

rule r1_1(reg_ref_clk == 1);
bfr5._tx_sof_n(False);
bfr5._tx_eof_n(True);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'hF52AD3E66EFC11375A8D9C8EFC0ACEA0);
//bfr5._tx_rem(4'b1111);
//bfr5._tx_crf(2'b11);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r1_2(reg_ref_clk > 1 && reg_ref_clk <= 17);
bfr5._tx_sof_n(True);
bfr5._tx_eof_n(True);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//bfr5._tx_rem(4'b1111);
//bfr5._tx_crf(2'b11);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r1_3(reg_ref_clk == 18);
bfr5._tx_sof_n(True);
bfr5._tx_eof_n(False);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'h6EFC11375A8D9C8EFC0ACEA0F52AD3E6);
bfr5._tx_rem(4'b1111);
bfr5._tx_crf(2'b11);
$display("Regstr value is %h",bfr5.buf_out_());
//$display ("rg_buf[2311] == %b", ]);
endrule

rule r1_3_1(reg_ref_clk == 19);
//bfr5._tx_vld_n(False);
/*
bfr3._tx_eof_n(False);
bfr3._tx_vld_n(False);
bfr3._tx_data(128'hCEA0F52AD3E66EFC11375A8D9C8EFC0A);
bfr3._tx_rem(4'b111);
bfr3._tx_crf(2'b11);*/
//bfr5._tx_eof_n(False);
//bfr5._tx_vld_n(False);
$display("Regstr value is %h",bfr5.buf_out_());
endrule


rule r1_3_2(reg_ref_clk == 20);
//bfr5._tx_wtr(1'b1);
//sof <= bfr5.lnk_tsof_n_();
//eof <= bfr5.lnk_teof_n_();
//if(sof == True)
bfr5._tx_read(1'b1);
$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("eof is %b",bfr5.lnk_teof_n_());
$display("vld is %b",bfr5.lnk_tvld_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());

//bfr5._tx_eof_n(False);
//bfr5._tx_vld_n(False);
//$display("Regstr value is %h",bfr5.buf_out_());
endrule
rule r1_3_3(reg_ref_clk >= 21 && reg_ref_clk<=37);
//bfr5._tx_wtr(1'b0);
$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("vld is %b",bfr5.lnk_tvld_n_());
$display("eof is %b",bfr5.lnk_teof_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());
endrule

rule r1_3_4_stop(reg_ref_clk == 23);
//bfr5._tx_wtr(1'b0);
bfr5._tx_read(1'b0);
//$display("sof is %b",bfr5.lnk_tsof_n_());
//else
//$display("vld is %b",bfr5.lnk_tvld_n_());
//$display("eof is %b",bfr5.lnk_teof_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

//$display("DATA is %h",bfr5.lnk_td_());
//$display("Rem is %b",bfr5.lnk_trem_());
//$display("CRF is %b",bfr5.lnk_tcrf_());
endrule

rule r1_3_4_restrt(reg_ref_clk == 26);
//bfr5._tx_wtr(1'b0);
bfr5._tx_read(1'b1);
//$display("sof is %b",bfr5.lnk_tsof_n_());
//else
//$display("vld is %b",bfr5.lnk_tvld_n_());
//$display("eof is %b",bfr5.lnk_teof_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

//$display("DATA is %h",bfr5.lnk_td_());
//$display("Rem is %b",bfr5.lnk_trem_());
//$display("CRF is %b",bfr5.lnk_tcrf_());
endrule

rule r1_3_4(reg_ref_clk == 38);
//bfr5._tx_wtr(1'b0);

$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("vld is %b",bfr5.lnk_tvld_n_());
$display("eof is %b",bfr5.lnk_teof_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());
endrule

rule r1_3_5(reg_ref_clk == 39);
//bfr5._tx_wtr(1'b0);
$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("vld is %b",bfr5.lnk_tvld_n_());
$display("eof is %b",bfr5.lnk_teof_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());
endrule

rule r1_3_6(reg_ref_clk == 40);
bfr5._tx_deq(1'b1);
//$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r1_3_7(reg_ref_clk == 41);
//bfr5._tx_deq(1'b1);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r1_3_8(reg_ref_clk == 42);
//bfr5._tx_deq(1'b0);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r1_4_8(reg_ref_clk == 43);
bfr5._tx_deq(1'b0);
//$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r1_1_1(reg_ref_clk == 44);
bfr5._tx_sof_n(False);
bfr5._tx_eof_n(False);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'hF52AD3E66EFC11375A8D9C8EFC0ACEA0);
bfr5._tx_rem(4'b1111);
bfr5._tx_crf(2'b11);
$display("Regstr value is %h",bfr5.buf_out_());
endrule


rule r1_1_2(reg_ref_clk == 45);
//bfr5._tx_sof_n(False);
//bfr5._tx_eof_n(False);
//bfr5._tx_vld_n(False);
//bfr5._tx_data(128'hF52AD3E66EFC11375A8D9C8EFC0ACEA0);
//bfr5._tx_rem(4'b1111);
//bfr5._tx_crf(2'b11);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

//rule r1_3_9(reg_ref_clk == 46);
//bfr5._tx_deq(1'b1);
//$display("Regstr value is %h",bfr5.buf_out_());
//endrule

rule r2_1(reg_ref_clk == 46);
bfr5._tx_sof_n(False);
bfr5._tx_eof_n(True);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EFC);
//bfr5._tx_rem(4'b1111);
//bfr5._tx_crf(2'b11);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r2_2(reg_ref_clk == 47);
bfr5._tx_sof_n(True);
bfr5._tx_eof_n(True);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//bfr5._tx_rem(4'b1111);
//bfr5._tx_crf(2'b11);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r2_4_8(reg_ref_clk == 48);
//bfr5._tx_deq(1'b0);
$display("Regstr value is %h",bfr5.buf_out_());
endrule

rule r2_3(reg_ref_clk == 49);
bfr5._tx_sof_n(True);
bfr5._tx_eof_n(False);
bfr5._tx_vld_n(False);
bfr5._tx_data(128'h6EFC11375A8D9C8EFC0ACEA0F52AD3E6);
bfr5._tx_rem(4'b0111);
bfr5._tx_crf(2'b00);
$display("Regstr value is %h",bfr5.buf_out_());
//$display ("rg_buf[2311] == %b", ]);
endrule

rule r2_3_8(reg_ref_clk == 50);
//bfr5._tx_deq(1'b0);
$display("Regstr value is %h",bfr5.buf_out_());
endrule


/*rule r1_3(reg_ref_clk == 20);
sof <= bfr5.lnk_tsof_n_();
eof <= bfr5.lnk_teof_n_();
if(sof == True)
$display("sof is TRUE");
else
$display("sof is FALSE");
if(eof == True)
$display("eof is TRUE");
else
$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());

//$display("Regstr value is %h",bfr5.buf_out_());


//bfr3._tx_crf(2'b11);
//bfr3._tx_eof_n(False);
//bfr3._tx_vld_n(False);
//$display("Regstr value is %h",bfr3.buf_out_());
endrule

rule r1_4(reg_ref_clk == 22);
//bfr5._tx_wtr(1'b1);
$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("eof is %b",bfr5.lnk_teof_n_());
$display("vld is %b",bfr5.lnk_tvld_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());

//$display("Regstr value is %h",bfr5.buf_out_());
//$display("Regstr value is %h",bfr5.buf_out_());

//bfr3._tx_crf(2'b11);
//bfr3._tx_eof_n(False);
//bfr3._tx_vld_n(False);
//$display("Regstr value is %h",bfr3.buf_out_());
endrule

rule r1_5(reg_ref_clk == 23);
bfr5._tx_wtr(1'b0);
$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("eof is %b",bfr5.lnk_teof_n_());
$display("vld is %b",bfr5.lnk_tvld_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());

//$display("Regstr value is %h",bfr5.buf_out_());

//bfr3._tx_crf(2'b11);
//bfr3._tx_eof_n(False);
//bfr3._tx_vld_n(False);
//$display("Regstr value is %h",bfr3.buf_out_());
endrule


rule r1_6(reg_ref_clk == 24);
//bfr5._tx_wtr(1'b0);
$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("eof is %b",bfr5.lnk_teof_n_());
$display("vld is %b",bfr5.lnk_tvld_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());


//$display("Regstr value is %h",bfr5.buf_out_());

//bfr3._tx_crf(2'b11);
//bfr3._tx_eof_n(False);
//bfr3._tx_vld_n(False);
//$display("Regstr value is %h",bfr3.buf_out_());
endrule

rule r1_7(reg_ref_clk == 25);
//bfr5._tx_wtr(1'b0);
$display("sof is %b",bfr5.lnk_tsof_n_());
//else
$display("eof is %b",bfr5.lnk_teof_n_());
$display("vld is %b",bfr5.lnk_tvld_n_());
//if(eof == True)
//$display("eof is TRUE");
//else
//$display("eof is FALSE");

$display("DATA is %h",bfr5.lnk_td_());
$display("Rem is %b",bfr5.lnk_trem_());
$display("CRF is %b",bfr5.lnk_tcrf_());


//$display("Regstr value is %h",bfr5.buf_out_());

//bfr3._tx_crf(2'b11);
//bfr3._tx_eof_n(False);
//bfr3._tx_vld_n(False);
//$display("Regstr value is %h",bfr3.buf_out_());
endrule

*/


endmodule:mkTb_RapidIOPhy_Buffer5
endpackage:Tb_RapidIOPhy_Buffer5
