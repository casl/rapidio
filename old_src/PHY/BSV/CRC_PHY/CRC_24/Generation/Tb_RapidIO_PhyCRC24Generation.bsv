/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Physical Layer CRC-24 Generation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- 1. It is used to test the CRC-24 generation.
-- 2. The control symbol packet (38 bits) is used as input data. 
-- 3. It is connected to CRC-24 generation module.
--  
-- 
--
-- Author(s):
-- Gopinathan (gopinathan18@gmail.com)
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_PhyCRC24Generation;

import RapidIO_PhyCRC24Generation ::*;
import LFSR:: *;


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkTb_RapidIO_PhyCRC24Generation(Empty);

Ifc_RapidIO_PhyCRC24Generation crc_24 <- mkRapidIO_PhyCRC24Generation;

LFSR#(Bit#(38)) lfsr <- mkFeedLFSR(38'b11000101010101110001010101010111111000);//polynomial for lfsr

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);
Reg#(Bit#(24)) crc <- mkReg (0);
/*
-- Following rule, it is used to generate reference clock 
*/
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 10)
		$finish (0);
endrule


rule r1_lfsr_start(reg_ref_clk == 1);
lfsr.seed(38'h3AAE85A770);
endrule:r1_lfsr_start


rule r1(reg_ref_clk == 2); 
crc_24._inputs_ControlSymbolData_in(lfsr.value);
crc_24._inputs_tx_vld (False);
$display("Input 1 = %b" , lfsr.value);
lfsr.next;
endrule:r1


rule r2(reg_ref_clk == 3); 
crc_24._inputs_ControlSymbolData_in(lfsr.value);
crc_24._inputs_tx_vld (False);
$display("Input 2 = %b" , lfsr.value);
lfsr.next;
endrule:r2


rule r2_1(reg_ref_clk == 4); 
crc_24._inputs_ControlSymbolData_in(lfsr.value);
crc_24._inputs_tx_vld (False);
$display("Input 3 = %b" , lfsr.value);
lfsr.next;
endrule:r2_1


rule r2_2(reg_ref_clk == 5); 
crc_24._inputs_ControlSymbolData_in(lfsr.value);
crc_24._inputs_tx_vld (False);
$display("Input 4 = %b" , lfsr.value);
lfsr.next;
endrule:r2_2


rule r2_3(reg_ref_clk == 6); 
crc_24._inputs_ControlSymbolData_in(lfsr.value);
crc_24._inputs_tx_vld (False);
$display("Input 5 = %b" , lfsr.value);
lfsr.next;
endrule:r2_3

 
rule r3;
$display("CRC24 code=%b",crc_24.outputs_CRC24_());
endrule:r3

endmodule: mkTb_RapidIO_PhyCRC24Generation
endpackage: Tb_RapidIO_PhyCRC24Generation
