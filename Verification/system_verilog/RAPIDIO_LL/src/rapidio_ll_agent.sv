/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

`ifndef RAPIDIO_AGENT_SVH
`define RAPIDIO_AGENT_SVH

// CLASS: rapidio_ll_agent
class rapidio_ll_agent extends uvm_agent;
 
  rapidio_ll_driver                rapidio_ll_bfm;
  rapidio_ll_sequencer          rapidio_ll_seqr;
  rapidio_ll_mon            rapidio_ll_monitor;
  rapidio_adapter             rap_ll_adp; 

  uvm_tlm_fifo #(byte unsigned)    ingress_ll_pkt_fifo; 
  uvm_tlm_fifo #(int)              ingress_ll_cntrl_fifo;

  // Config class
  rapidio_ll_agent_config       rapidio_ll_cfg;

  bit                       is_initiator = 1'b0;
  // Virtual interface declaration
  virtual interface         rapidio_interface  vif;

  // Analysis port declarations
  uvm_analysis_port#(rapidio_ll_sequence_item)   cov_port;    // for coverage
  uvm_analysis_port#(rapidio_ll_sequence_item)   sb_port;     // for scoreboard

  // UVM automation macros
  `uvm_component_utils_begin(rapidio_ll_agent) 
    `uvm_field_object(rapidio_ll_bfm, UVM_ALL_ON)
    `uvm_field_object(rapidio_ll_seqr, UVM_ALL_ON)
    `uvm_field_object(rapidio_ll_monitor, UVM_ALL_ON)
    `uvm_field_object(rapidio_ll_cfg, UVM_ALL_ON)
    `uvm_field_object(rap_ll_adp, UVM_ALL_ON) // RAL CODE //
    `uvm_field_int(is_initiator, UVM_ALL_ON)
  `uvm_component_utils_end
  
  // Declare functions & tasks
  extern function new (string name, uvm_component parent);
  extern function void build_phase(uvm_phase phase);
  extern function void connect_phase(uvm_phase phase);   
//  extern protected task assertion_enable_control(rapidio_assertion_enables rapidio_assert_enables, rapidio_ll_agent_config rapidio_cfg);

  task run_phase(uvm_phase phase);
  endtask : run_phase

endclass : rapidio_ll_agent

// FUNC : new - constructor
function rapidio_ll_agent::new (string name, uvm_component parent);
  super.new(name, parent);
  ingress_ll_pkt_fifo = new("ingress_ll_pkt_fifo",this,0);
  ingress_ll_cntrl_fifo = new("ingress_ll_cntrl_fifo",this,0);
  cov_port = new("cov_port",this);
  sb_port = new("sb_port",this);
endfunction : new

// FUNC : build_phase
function void rapidio_ll_agent::build_phase(uvm_phase phase);
  super.build_phase(phase);
  rapidio_ll_monitor = rapidio_ll_mon::type_id::create("rapidio_ll_monitor", this);
  rap_ll_adp = rapidio_adapter::type_id::create("rap_ll_adp",this);  // RAL CODE //
  if(!uvm_config_db#(virtual rapidio_interface)::get(this, "", "vif", vif))
    `uvm_fatal("RAPIDIO_LL_AGNT_FATAL",{"VIRTUAL INTERFACE MUST BE SET FOR: ",get_full_name(),".vif"});
  if(is_initiator == 1)begin

    void'(uvm_config_db#(rapidio_ll_agent_config)::get(this,"","rapidio_ll_agent_config",rapidio_ll_cfg));  // Getting initiator config from env
      rapidio_ll_seqr     = rapidio_ll_sequencer::type_id::create("rapidio_ll_seqr", this);
      rapidio_ll_bfm = rapidio_ll_driver::type_id::create("rapidio_ll_bfm", this);
    rapidio_ll_monitor.initiator_enable = 1;
  end
  else if(is_initiator == 0)begin
    void'(uvm_config_db#(rapidio_ll_agent_config)::get(this,"","rapidio_ll_agent_config",rapidio_ll_cfg)); // Getting target config from env
    if(rapidio_ll_cfg.is_active == UVM_ACTIVE) begin
      rapidio_ll_seqr     = rapidio_ll_sequencer::type_id::create("rapidio_ll_seqr", this);
      rapidio_ll_bfm = rapidio_ll_driver::type_id::create("rapidio_ll_bfm", this);
    end
    if(rapidio_ll_cfg.has_coverage) begin
    end
    rapidio_ll_monitor.target_enable = 1;
    rapidio_ll_cfg.check_target_config();
  end
endfunction : build_phase

// FUNC : connect_phase
function void rapidio_ll_agent::connect_phase(uvm_phase phase);
      rapidio_ll_bfm.seq_item_port.connect(rapidio_ll_seqr.seq_item_export);
      rapidio_ll_seqr.response_ph_port.connect(rapidio_ll_monitor.response_ph_imp_port);        // Peek port
      rapidio_ll_monitor.cov_port.connect(this.cov_port); 
      rapidio_ll_monitor.get_pkt_port.connect(ingress_ll_pkt_fifo.get_export);	
      rapidio_ll_monitor.get_cntrl_port.connect(ingress_ll_cntrl_fifo.get_export);	
  if(is_initiator == 1)begin
  end
  else if(is_initiator == 0)begin
  end
endfunction : connect_phase

`endif
