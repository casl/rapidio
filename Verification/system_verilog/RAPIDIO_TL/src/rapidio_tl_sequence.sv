/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_TL_SEQUENCE_SVH
`define RAPIDIO_TL_SEQUENCE_SVH

// Virtual Sequence base class
  class rapidio_tl_sequence_base extends uvm_sequence #(rapidio_tl_sequence_item);

  // UVM automation utils
  `uvm_object_utils(rapidio_tl_sequence_base)

  // Create handle for parent sequencer
 // rapidio_virtual_sequencer rio_v_seqr;
  // Create hanlde for sub sequencers
  rapidio_tl_sequencer rio_tl_seqr;

  rapidio_tl_agent_config rio_tl_cfg;
  rapidio_tl_sequence_item tl_seq_item;

  // Object constructor //
  function new(string name = "rapidio_tl_sequence_base");
    super.new(name);
  endfunction : new

  // Assigning pointers to sub-handlers
  virtual task body();
    if(!$cast(rio_tl_seqr,m_sequencer)) begin
      `uvm_error(get_full_name(),"Virtual sequencer pinter cast failed");
    end
  //  rio_tl_seqr = rio_v_seqr.rio_tl_seqr;
  endtask: body

 endclass: rapidio_tl_sequence_base

// SEQUENCE: 8 bit source id/ destination id seq  
class rapidio_8bit_read_write_seq extends rapidio_tl_sequence_base;

  rapidio_tl_agent_config rio_tl_cfg;

  // memory declaration
  bit [7:0]  memory[*];

  // FUNC : Constructor
  function new(string name="rapidio_8bit_read_write_seq");
    super.new(name);
  endfunction: new

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_8bit_read_write_seq)
  `uvm_object_utils_end
 
  // TASK : body
  virtual task body();
  // Body
    `uvm_do_with (tl_seq_item, {tt == 2'b00;hop_count==8'h00;});  
  endtask :body
endclass :rapidio_8bit_read_write_seq


// SEQUENCE: 16 bit source id/ destination id seq  
class rapidio_16bit_read_write_seq extends rapidio_tl_sequence_base;

  rapidio_tl_agent_config rio_tl_cfg;

  // memory declaration
  bit [15:0]  memory[*];

  // FUNC : Constructor
  function new(string name="rapidio_16bit_read_write_seq");
    super.new(name);
  endfunction: new

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_16bit_read_write_seq)
  `uvm_object_utils_end
 
  // TASK : body
  virtual task body();
  // Body
    `uvm_do_with (tl_seq_item, {tt == 2'b01;hop_count==8'h00;});  
  endtask :body
endclass :rapidio_16bit_read_write_seq


// SEQUENCE: 32 bit source id/ destination id seq  
class rapidio_32bit_read_write_seq extends rapidio_tl_sequence_base;

  rapidio_tl_agent_config rio_tl_cfg;

  // memory declaration
  bit [31:0]  memory[*];

  // FUNC : Constructor
  function new(string name="rapidio_32bit_read_write_seq");
    super.new(name);
  endfunction: new

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_32bit_read_write_seq)
  `uvm_object_utils_end
 
  // TASK : body
  virtual task body();
  // Body
    `uvm_do_with (tl_seq_item, {tt == 2'b10;hop_count==8'h00;});  
  endtask :body
endclass :rapidio_32bit_read_write_seq

// SEQUENCE:   
class rapidio_read_write_rand_seq extends rapidio_tl_sequence_base;

  rapidio_tl_agent_config rio_tl_cfg;

  // memory declaration
  bit [31:0]  memory[*];

  // FUNC : Constructor
  function new(string name="rapidio_32bit_read_write_seq");
    super.new(name);
  endfunction: new

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_32bit_read_write_seq)
  `uvm_object_utils_end
 
  // TASK : body
  virtual task body();
  // Body
    `uvm_do(tl_seq_item);  
  endtask :body
endclass :rapidio_read_write_rand_seq



`endif

