/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

class DEVICE_IDENTITY_CAR extends uvm_reg ;
    
  // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(DEVICE_IDENTITY_CAR)

  // Declare the integrals //
    rand uvm_reg_field DeviceIdentity;
    rand uvm_reg_field DeviceVendorIdentity;
    
  // Construct the base class //
   function new(string name = "DEVICE_IDENTITY_CAR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   $display("This is the build function of the device identity car ");
   DeviceIdentity        =  uvm_reg_field::type_id::create("DeviceIdentity"); 
   DeviceVendorIdentity  =  uvm_reg_field::type_id::create("DeviceVendorIdentity");  
   
   // Now congigure it //
   DeviceIdentity.configure(this,15,0,"RW",0,0,1'b1,1,0);
   DeviceVendorIdentity.configure(this,15,15,"RW",0,0,1'b1,1,0);
  endfunction 


function void dis_in_dev();
$display(">>>>>>>>>>>>>>>>>>>>In ral_reg");
endfunction

endclass


  // DEVICE INFORMATION CAR //
  class DEVICE_INFORMATION_CAR extends uvm_reg ;
    
  // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(DEVICE_INFORMATION_CAR)

  // Declare the integrals //
    rand uvm_reg_field DeviceRev;
    
  // Construct the base class //
   function new(string name = "DEVICE_INFORMATION_CAR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   DeviceRev        =  uvm_reg_field::type_id::create("DeviceRev"); 
   
   // Now congigure it //
   DeviceRev.configure(this,32,0,"RW",0,0,1'b1,1,0);
  endfunction 

endclass


  // ASSEMBLY IDENTITY CAR //
  class ASSEMBLY_IDENTITY_CAR extends uvm_reg ;
    
  // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(ASSEMBLY_IDENTITY_CAR)

  // Declare the integrals //
    rand uvm_reg_field AssemblyIdentity;
    rand uvm_reg_field AssemblyVendorIdentity;
    
  // Construct the base class //
   function new(string name = "ASSEMBLY_IDENTITY_CAR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
    AssemblyIdentity       =  uvm_reg_field::type_id::create("AssemblyIdentity"); 
    AssemblyVendorIdentity =  uvm_reg_field::type_id::create("AssemblyVendorIdentity");  
   
   // Now congigure it //
   AssemblyIdentity.configure(this,15,0,"RW",0,0,1'b1,1,0);
   AssemblyVendorIdentity.configure(this,15,15,"RW",0,0,1'b1,1,0);
  endfunction 

endclass


  // DEVICE INFORMATION CAR //
  class ASSEMBLY_INFORMATION_CAR extends uvm_reg ;
    
  // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(ASSEMBLY_INFORMATION_CAR)

  // Declare the integrals //
    rand uvm_reg_field AssyRev;
    rand uvm_reg_field ExtendedFeatureptr;
    
  // Construct the base class //
   function new(string name = "ASSEMBLY_INFORMATION_CAR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
    AssyRev       =  uvm_reg_field::type_id::create("AssyRev"); 
    ExtendedFeatureptr =  uvm_reg_field::type_id::create("ExtendedFeatureptr");  
   
   // Now congigure it //
   AssyRev.configure(this,15,0,"RW",0,0,1'b1,1,0);
   ExtendedFeatureptr.configure(this,15,15,"RW",0,0,1'b1,1,0);
  endfunction 

endclass


// PROCESSING ELEMENT FEATURE //
class PROCESSING_ELEMENT_FEATURE extends uvm_reg;

 // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(PROCESSING_ELEMENT_FEATURE)

  // Declare the integrals //
    rand uvm_reg_field Bridge;
    rand uvm_reg_field Memory;
    rand uvm_reg_field Processor;
    rand uvm_reg_field Switch;
    rand uvm_reg_field Reserved;
    rand uvm_reg_field Extended_features;
    rand uvm_reg_field Extended_Addressing_Support;
    
  // Construct the base class //
   function new(string name = "PROCESSING_ELEMENT_FEATURE");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
    Bridge=  uvm_reg_field::type_id::create("Bridge"); 
    Memory=  uvm_reg_field::type_id::create("Memory");  
    Processor=  uvm_reg_field::type_id::create("Processor");  
    Switch=  uvm_reg_field::type_id::create("Switch");  
    Reserved =  uvm_reg_field::type_id::create("Reserved");  
    Extended_features =  uvm_reg_field::type_id::create("Extended_features");  
    Extended_Addressing_Support=  uvm_reg_field::type_id::create("Extended_Addressing_Support");  
   
   // Now congigure it //
   Bridge.configure(this,1,0,"RW",0,0,1'b1,1,1);
 Memory.configure(this,1,1,"RW",0,0,1'b1,1,1);
   Processor.configure(this,1,2,"RW",0,0,1'b1,1,0);
   Switch .configure(this,1,3,"RW",0,0,1'b1,1,0);
   Reserved.configure(this,24,4,"RW",0,0,1'b1,1,0);
   Extended_features.configure(this,1,28,"RW",0,0,1'b1,1,0);
   Extended_Addressing_Support.configure(this,2,29,"RW",0,0,1'b1,1,0);
  endfunction 

endclass


 //SWITCH_PORT_INFORMATION  //
class SWITCH_PORT_INFORMATION extends uvm_reg;

 // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(SWITCH_PORT_INFORMATION)

  // Declare the integrals //
    rand uvm_reg_field Reserved;
    rand uvm_reg_field PortTotal;
    rand uvm_reg_field PortNumber;
        
  // Construct the base class //
   function new(string name = "SWITCH_PORT_INFORMATION");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   Reserved =  uvm_reg_field::type_id::create("Reserved"); 
   PortTotal =  uvm_reg_field::type_id::create("PortTotal");  
   PortNumber =  uvm_reg_field::type_id::create("PortNumber");  
      
   
   // Now congigure it //
   Reserved.configure(this,16,0,"RW",0,0,1'b1,1,0);
   PortTotal.configure(this,8,16,"RW",0,0,1'b1,1,0);
   PortNumber.configure(this,8,24,"RW",0,0,1'b1,1,0);
   endfunction 

endclass


 // SOURCE OPERATIONS CAR //
class SOURCE_OPERATIONS_CAR extends uvm_reg;

 // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(SOURCE_OPERATIONS_CAR)

  // Declare the integrals //
    rand uvm_reg_field Reserved0;
    rand uvm_reg_field Implementation_Defined0;
    rand uvm_reg_field Read;
    rand uvm_reg_field Write;
    rand uvm_reg_field Streaming_write;
    rand uvm_reg_field Reserved1;
    rand uvm_reg_field Atomic_compare_and_swap ;
    rand uvm_reg_field Atomic_test_and_swap ;
    rand uvm_reg_field Atomic_increament ;
    rand uvm_reg_field Atomic_decreament ;
    rand uvm_reg_field Atomic_set ;
    rand uvm_reg_field Atomic_clear ;
    rand uvm_reg_field Atomic_swap ;
    rand uvm_reg_field Atomic_port_write ;
    rand uvm_reg_field Implementation_Defined1;
    
  // Construct the base class //
   function new(string name = "SOURCE_OPERATIONS_CAR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   Reserved0 =  uvm_reg_field::type_id::create("Reserved0"); 
   Implementation_Defined0=  uvm_reg_field::type_id::create("Implementation_Defined0");  
   Read =  uvm_reg_field::type_id::create("Read");  
   Write=  uvm_reg_field::type_id::create("Write");  
   Streaming_write=  uvm_reg_field::type_id::create("Streaming_write");  
   Reserved1 =  uvm_reg_field::type_id::create("Reserved1");  
   Atomic_compare_and_swap =  uvm_reg_field::type_id::create("Atomic_compare_and_swap ");  
   Atomic_test_and_swap=  uvm_reg_field::type_id::create("Atomic_test_and_swap");  
   Atomic_increament=  uvm_reg_field::type_id::create("Atomic_increament");  
   Atomic_decreament=  uvm_reg_field::type_id::create("Atomic_decreament");  
   Atomic_set =  uvm_reg_field::type_id::create("Atomic_set");  
   Atomic_clear=  uvm_reg_field::type_id::create("Atomic_clear");  
   Atomic_swap =  uvm_reg_field::type_id::create("Atomic_swap");  
   Implementation_Defined1=  uvm_reg_field::type_id::create("Implementation_Defined1");  
   Atomic_port_write=  uvm_reg_field::type_id::create("Atomic_port_write");  
   $display( " YES IT IS THE SOURCE OPERATION REGISTER \n"); 
   // Now congigure it //
    Reserved0.configure(this,14,0,"RW",0,0,1'b1,1,0);
    Implementation_Defined0.configure(this,2,14,"RW",0,0,1'b1,1,0);
    Read.configure(this,1,16,"RW",0,0,1'b1,1,0);
    Write.configure(this,1,17,"RW",0,0,1'b1,1,0);
    Streaming_write.configure(this,1,18,"RW",0,0,1'b1,1,0);
    Reserved1.configure(this,2,19,"RW",0,0,1'b1,1,0);
    Atomic_compare_and_swap.configure(this,1,21,"RW",0,0,1'b1,1,0);
    Atomic_test_and_swap.configure(this,1,22,"RW",0,0,1'b1,1,0);
    Atomic_increament.configure(this,1,23,"RW",0,0,1'b1,1,0);
    Atomic_decreament.configure(this,1,24,"RW",0,0,1'b1,1,0);
    Atomic_set.configure(this,1,25,"RW",0,0,1'b1,1,0);
    Atomic_clear.configure(this,1,26,"RW",0,0,1'b1,1,0);
    Atomic_swap.configure(this,1,27,"RW",0,0,1'b1,1,0);
    Atomic_port_write.configure(this,1,28,"RW",0,0,1'b1,1,0);
    Implementation_Defined1.configure(this,2,30,"RW",0,0,1'b1,1,0);
  endfunction 

endclass


 // DESTINATION OPERATIONS CAR //
class DESTINATION_OPERATIONS_CAR extends uvm_reg;

 // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(DESTINATION_OPERATIONS_CAR)

  // Declare the integrals //
    rand uvm_reg_field Reserved0;
    rand uvm_reg_field Implementation_Defined0;
    rand uvm_reg_field Read;
    rand uvm_reg_field Write;
    rand uvm_reg_field Streaming_write;
    rand uvm_reg_field Reserved1;
    rand uvm_reg_field Atomic_compare_and_swap ;
    rand uvm_reg_field Atomic_test_and_swap ;
    rand uvm_reg_field Atomic_increament ;
    rand uvm_reg_field Atomic_decreament ;
    rand uvm_reg_field Atomic_set ;
    rand uvm_reg_field Atomic_clear ;
    rand uvm_reg_field Atomic_swap ;
    rand uvm_reg_field Atomic_port_write ;
    rand uvm_reg_field Implementation_Defined1; 
    
  // Construct the base class //
   function new(string name = "DESTINATION_OPERATIONS_CAR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   Reserved0 =  uvm_reg_field::type_id::create("Reserved0"); 
   Implementation_Defined0=  uvm_reg_field::type_id::create("Implementation_Defined0");  
   Read =  uvm_reg_field::type_id::create("Read");  
   Write=  uvm_reg_field::type_id::create("Write");  
   Streaming_write=  uvm_reg_field::type_id::create("Streaming_write");  
   Reserved1 =  uvm_reg_field::type_id::create("Reserved1");  
   Atomic_compare_and_swap =  uvm_reg_field::type_id::create("Atomic_compare_and_swap ");  
   Atomic_test_and_swap=  uvm_reg_field::type_id::create("Atomic_test_and_swap");  
   Atomic_increament=  uvm_reg_field::type_id::create("Atomic_increament");  
   Atomic_decreament=  uvm_reg_field::type_id::create("Atomic_decreament");  
   Atomic_set =  uvm_reg_field::type_id::create("Atomic_set");  
   Atomic_clear=  uvm_reg_field::type_id::create("Atomic_clear");  
   Atomic_swap =  uvm_reg_field::type_id::create("Atomic_swap");  
   Implementation_Defined1=  uvm_reg_field::type_id::create("Implementation_Defined1");  
   Atomic_port_write=  uvm_reg_field::type_id::create("Atomic_port_write");  
   $display( " YES IT IS THE DESTINATION OPERATION REGISTER \n"); 
   
   // Now congigure it //
    Reserved0.configure(this,14,0,"RW",0,0,1'b1,1,0);
    Implementation_Defined0.configure(this,2,14,"RW",0,0,1'b1,1,0);
    Read.configure(this,1,16,"RW",0,0,1'b1,1,0);
    Write.configure(this,1,17,"RW",0,0,1'b1,1,0);
    Streaming_write.configure(this,1,18,"RW",0,0,1'b1,1,0);
    Reserved1.configure(this,2,19,"RW",0,0,1'b1,1,0);
    Atomic_compare_and_swap.configure(this,1,21,"RW",0,0,1'b1,1,0);
    Atomic_test_and_swap.configure(this,1,22,"RW",0,0,1'b1,1,0);
    Atomic_increament.configure(this,1,23,"RW",0,0,1'b1,1,0);
    Atomic_decreament.configure(this,1,24,"RW",0,0,1'b1,1,0);
    Atomic_set.configure(this,1,25,"RW",0,0,1'b1,1,0);
    Atomic_clear.configure(this,1,26,"RW",0,0,1'b1,1,0);
    Atomic_swap.configure(this,1,27,"RW",0,0,1'b1,1,0);
    Atomic_port_write.configure(this,1,28,"RW",0,0,1'b1,1,0);
    Implementation_Defined1.configure(this,2,29,"RW",0,0,1'b1,1,0);
  endfunction 

endclass


  // PROCESSING ELEMENT LOGICAL LAYER CONTROL CSR //
  class PROCESSING_ELEMENT_LOGICAL_LAYER_CONTROL_CSR extends uvm_reg;

 // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(PROCESSING_ELEMENT_LOGICAL_LAYER_CONTROL_CSR)

  // Declare the integrals //
    rand uvm_reg_field Reserved;
    rand uvm_reg_field Extended_Addressing_control;
        
  // Construct the base class //
   function new(string name = "PROCESSING_ELEMENT_LOGICAL_LAYER_CONTROL_CSR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   Reserved =  uvm_reg_field::type_id::create("Reserved"); 
   Extended_Addressing_control =  uvm_reg_field::type_id::create("Extended_Addressing_control");  
   
   // Now congigure it //
   Reserved.configure(this,29,0,"RW",0,0,1'b1,1,0);
   Extended_Addressing_control.configure(this,3,29,"RW",0,0,1'b1,1,0);
   endfunction 

 endclass


 // LOCAL CONFIGURATOIN SPACE BASE ADDRESS 0 CSR //
  class LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_0_CSR extends uvm_reg;

 // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_0_CSR)

  // Declare the integrals //
    rand uvm_reg_field Reserved;
    rand uvm_reg_field LCBSA0;
    rand uvm_reg_field LCBSA1;
        
  // Construct the base class //
   function new(string name = "LOCAL_CONFIGURATOIN_SPACE_BASE_ADDRESS_0_CSR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   Reserved =  uvm_reg_field::type_id::create("Reserved"); 
   LCBSA0 =  uvm_reg_field::type_id::create("LCBSA0");  
   LCBSA1 =  uvm_reg_field::type_id::create("LCBSA0");  
      
   
   // Now congigure it //
   Reserved.configure(this,1,0,"RW",0,0,1'b1,1,0);
   LCBSA0.configure(this,15,1,"RW",0,0,1'b1,1,0);
   LCBSA1.configure(this,15,17,"RW",0,0,1'b1,1,0);
   endfunction 

  endclass


 // LOCAL CONFIGURATOIN SPACE BASE ADDRESS 1 CSR //
  class LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_1_CSR extends uvm_reg;

 // Register the object in the factory for future ovverriding purpose //
   `uvm_object_utils(LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_1_CSR)

  // Declare the integrals //
    rand uvm_reg_field LCBSA0;
    rand uvm_reg_field LCBSA1;
        
  // Construct the base class //
   function new(string name = "LOCAL_CONFIGURATI0N_SPACE_BASE_ADDRESS_1_CSR");
    super.new(name,32,UVM_NO_COVERAGE);  // Here 32 is the width of the bits //
   endfunction 

  // Build the register class //
  // This build method is mot called by the UVM_COMPONENT //
  virtual function void build();
   LCBSA0 =  uvm_reg_field::type_id::create("LCBSA0");  
   LCBSA1 =  uvm_reg_field::type_id::create("LCBSA0");  
      
   
   // Now congigure it //
   LCBSA0.configure(this,1,0,"RW",0,0,1'b1,1,0);
   LCBSA1.configure(this,31,1,"RW",0,0,1'b1,1,0);
   endfunction 

 endclass



