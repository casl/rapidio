### Makefile for the srio

SRIO_HOME=$(PWD)
export SRIO_HOME

###### Setting the variables for bluespec compile #$############################
TOP_MODULE:=mksrio_txrx_txblock_ll_ireq
TOP_FILE:=srio_txrx_txblock_ll_ireq.bsv
TOP_DIR:= $(SRIO_HOME)/src
BSVCOMPILEOPTS:= -keep-fires 
BSVLINKOPTS:=-parallel-sim-link 8 -keep-fires
BSVBUILDDIR:=$(SRIO_HOME)/bsv_build/
VERILOGDIR:=$(SRIO_HOME)/verilog/
BSVINCDIR:= .:%/Prelude:%/Libraries:%/Libraries/BlueNoC
BSVOUTDIR:=$(SRIO_HOME)/bin

FILES:= $(SRIO_HOME)/src/
BSVINCDIR:= .:%/Prelude:%/Libraries:%/Libraries/BlueNoC:$(FILES)
default: full_clean compile link simulate

.PHONY: compile_bluesim
compile_bluesim:
	@echo "Compiling $(TOP_MODULE) in Bluesim..."
	@mkdir -p $(BSVBUILDDIR)
	bsc -u -sim -simdir $(BSVBUILDDIR) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR) $(BSVCOMPILEOPTS) -p $(BSVINCDIR) -g $(TOP_MODULE)  $(TOP_DIR)/$(TOP_FILE) 2>&1 | tee bsv_compile.log
	@echo Compilation finished

.PHONY: link_bluesim
link_bluesim:
	@echo "Linking $(TOP_MODULE) in Bluesim..."
	@mkdir -p $(BSVOUTDIR)
	bsc -e $(TOP_MODULE) -sim -o $(BSVOUTDIR)/out -simdir $(BSVBUILDDIR) -p $(BSVINCDIR) -bdir $(BSVBUILDDIR) $(BSVLINKOPTS) 2>&1 | tee bsv_link.log
	@echo Linking finished

.PHONY: simulate
simulate:
	@echo Simulation...
	@exec $(SRIO_HOME)/$(BSVOUTDIR)/out | tee sim.log
	@echo Simulation finished

.PHONY: generate_verilog 
generate_verilog: 
	@echo Compiling $(TOP_MODULE) in Verilog for simulations ...
	@mkdir -p $(BSVBUILDDIR);
	@mkdir -p $(VERILOGDIR);
	bsc -u -verilog -elab -vdir $(VERILOGDIR) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR) -D verilog=True $(BSVCOMPILEOPTS) -verilog-filter ${BLUESPECDIR}/bin/basicinout -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE) 2>&1 | tee bsv_compile.log
	@echo Compilation finished

.PHONY: link_ncverilog
link_ncverilog: 
	@echo "Linking $(TOP_MODULE) using ncverilog..."
	@rm -rf work include bin/work
	@mkdir -p bin 
	@mkdir work
	@echo "define work ./work" > cds.lib
	@echo "define WORK work" > hdl.var
	@ncvlog -sv -cdslib ./cds.lib -hdlvar ./hdl.var +define+TOP=$(TOP_MODULE) ${BLUESPECDIR}/Verilog/main.v -y $(VERILOGDIR)/ -y ${BLUESPECDIR}/Verilog/ -y ./src/bfm
	@ncelab  -cdslib ./cds.lib -hdlvar ./hdl.var work.main -timescale 1ns/1ps
	@echo 'ncsim -cdslib ./cds.lib -hdlvar ./hdl.var work.main #> /dev/null' > $(BSVOUTDIR)/out
	@mv work cds.lib hdl.var $(BSVOUTDIR)/
	@chmod +x $(BSVOUTDIR)/out
	@echo Linking finished

.PHONY: clean
clean:
	rm -rf $(BSVBUILDDIR) *.log $(BSVOUTDIR) 	
	rm -rf verilog

.PHONY: full_clean
full_clean:
