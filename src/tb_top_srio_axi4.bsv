/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: M Gopinathan
Email id: gopinathan18@gmail.com
Details: Top - AXI Fabric

--------------------------------------------------------------------------------------------------
*/

package tb_top_srio_axi4;
`include "defines_srio.bsv"
import srio_axi4_slave::*;
import tb_srio_axi4_slave::*;
import Semi_FIFOF::*;
import AXI4_Types::*;
import AXI4_Fabric  :: *;
import Connectable ::*;
import DefaultValue ::*;


typedef 1 Num_Masters;
typedef 1 Num_Slaves;

function Tuple2 #(Bool, Bit#(TLog#(Num_Slaves))) fn_addr_to_slave_num  (Bit#(`ADDR) addr);
			return tuple2(True,0);
endfunction

(*synthesize*)
module mktb_top_srio_axi4(Empty);


Ifc_tb_srio_axi4_slave tb_master <- mktb_srio_axi4_slave();
Ifc_srio_axi4_slave tb_slave <- mksrio_axi4_slave();

AXI4_Fabric_IFC #(Num_Masters, Num_Slaves, `ADDR, `DATA,`USERSPACE) 
  fabric <- mkAXI4_Fabric(fn_addr_to_slave_num);

mkConnection (tb_master.srio_axi4_master,fabric.v_from_masters[0]);
mkConnection (fabric.v_to_slaves[0],tb_slave.srio_axi4_slave);

endmodule : mktb_top_srio_axi4
endpackage : tb_top_srio_axi4
