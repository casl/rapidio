/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: M Gopinathan
Email id: gopinathan18@gmail.com
Details: Testbench - AXI Master transactor

--------------------------------------------------------------------------------------------------
*/
package tb_srio_axi4_slave;
`include "defines_srio.bsv"
import Semi_FIFOF::*;
import AXI4_Types::*;
import AXI4_Fabric:: *;
import LFSR ::*;

typedef enum {Idle,
              Read_request,
              Read_response,
              Write_request,
              Write_response
} States deriving (Bits, Eq);

interface Ifc_tb_srio_axi4_slave;
 interface AXI4_Master_IFC#(`ADDR, `DATA, `USERSPACE) srio_axi4_master;
endinterface

module mktb_srio_axi4_slave (Ifc_tb_srio_axi4_slave);

AXI4_Master_Xactor_IFC#(`ADDR,`DATA,`USERSPACE) tb_srio_axi_xactor <- mkAXI4_Master_Xactor;
LFSR #(Bit #(32)) lfsr_data <- mkLFSR_32;
Reg#(States) rg_state <- mkReg(Idle);

// Read Request
rule rl_read_request(rg_state == Read_request);
  let lv_read_request =  AXI4_Rd_Addr{araddr:? ,arlen:? ,arsize:? ,arburst:?};
  tb_srio_axi_xactor.i_rd_addr.enq(lv_read_request);
endrule

// Read Response
rule rl_read_response(rg_state == Read_response);
  let lv_read_response <- pop_o(tb_srio_axi_xactor.o_rd_data);
  $display($stime(),": Read data %x ",lv_read_response.rdata);
endrule

// Nwrite operation write equest
rule rl_write_request(rg_state == Write_request);
  let lv_wr_data = {lfsr_data.value(),lfsr_data.value(),lfsr_data.value(),lfsr_data.value()};
  lfsr_data.next();
  let lv_aw = AXI4_Wr_Addr{awaddr: ? ,awlen: ? ,awsize: ?,awburst: ?};
  let lv_w = AXI4_Wr_Data {wdata: lv_wr_data, wstrb: ?, wlast: ? };
  tb_srio_axi_xactor.i_wr_addr.enq(lv_aw);
  tb_srio_axi_xactor.i_wr_data.enq(lv_w);
endrule


interface srio_axi4_master = tb_srio_axi_xactor.axi_side;

endmodule : mktb_srio_axi4_slave
endpackage : tb_srio_axi4_slave
